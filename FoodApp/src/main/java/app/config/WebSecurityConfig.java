package app.config;

import app.constants.Permissions;
import app.constants.URIMappings;
import app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ADMIN_USERNAME_FIELD = "username";
    private static final String ADMIN_PASSWORD_FIELD = "password";

    @Autowired
    private UserService userService;

    private AccessDeniedHandler accessDeniedHandler = new CustomAccessDeniedHandler(URIMappings.ACCESS_DENIED);;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers()
                .frameOptions().sameOrigin();

        http
                .csrf()
                .ignoringAntMatchers(URIMappings.URI_ARTICLE_PICTURE_UPLOAD)
                .ignoringAntMatchers(URIMappings.URI_RECIPES_PICTURE_UPLOAD);


        http
                .authorizeRequests()
                .antMatchers(URIMappings.URI_ADMIN_URL).access(Permissions.PERMISSION_IS_ADMIN)
                .and()
                .formLogin()
                .loginPage(URIMappings.URI_ADMIN_LOGIN)
                .loginProcessingUrl(URIMappings.URI_ADMIN_LOGIN)
                .defaultSuccessUrl(URIMappings.URI_ADMIN_MAIN_VIEW)
                .failureUrl(URIMappings.URI_ADMIN_LOGIN)
                .usernameParameter(ADMIN_USERNAME_FIELD)
                .passwordParameter(ADMIN_PASSWORD_FIELD)
                .permitAll()
                .and()
                .logout()
                .logoutUrl(URIMappings.URI_ADMIN_LOGOUT)
                .logoutSuccessUrl(URIMappings.URI_HOME_INDEX)
                .and()
                .rememberMe()
                .and()
                .exceptionHandling().accessDeniedHandler(this.accessDeniedHandler);
    }


    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }
}