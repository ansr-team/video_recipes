package app.config;

import app.constants.ComponentFolders;
import app.constants.Patterns;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.article.CreateArticleBindingModel;
import app.models.dto.bindingModel.article.EditArticleBindingModel;
import app.models.dto.bindingModel.category.CreateCategoryBindingModel;
import app.models.dto.bindingModel.category.EditCategoryBindingModel;
import app.models.dto.bindingModel.recipe.CreateRecipeBindingModel;
import app.models.dto.bindingModel.recipe.RecipeEditBindingModel;
import app.models.dto.bindingModel.video.CreateVideoPerCategoryBindingModel;
import app.models.dto.viewModel.article.ArticlePreviewViewModel;
import app.models.dto.viewModel.article.ArticleViewModel;
import app.models.dto.viewModel.article.EditArticleViewModel;
import app.models.dto.viewModel.article.GetArticleViewModel;
import app.models.dto.viewModel.category.CategoryViewModel;
import app.models.dto.viewModel.category.EditCategoryViewModel;
import app.models.dto.viewModel.category.ListCategoryViewModel;
import app.models.dto.viewModel.recipe.*;
import app.models.dto.viewModel.video.VideoSearchViewModel;
import app.models.dto.viewModel.video.VideoViewModel;
import app.models.entity.*;
import app.repositories.PictureRepository;
import app.services.article.ArticleService;
import app.services.category.CategoryService;
import app.services.recipe.RecipeService;
import app.services.video.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static app.constants.Patterns.REGEX_MATCH_HTML_ATTRIBUTES;

@Component
public class MagicMapperConfig {

    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private RecipeService recipeService;

    @Autowired
    private ArticleService articleService;

    private MagicMapper magicMapper;

    public MagicMapperConfig(MagicMapper magicMapper) {
        this.magicMapper = magicMapper;
        this.initialize();
    }

    private void initialize() {
        /**
         * Category
         */
        this.createCategoryInit();
        this.viewCategoryInit();
        this.createCategoryViewModelInit();
        this.createVideoPerCategoryInit();
        this.editCategoryInit();
        this.editCategoryViewModel();

        /**
         * Article
         */
        this.createArticleInit();
        this.editArticleInit();
        this.editArticleViewModel();
        this.createArticlePreviewViewModel();
        this.articleViewModel();
        this.articleGetViewModel();

        /**
         * Recipe
         */
        this.createRecipeInit();
        this.editRecipeViewModel();
        this.editRecipeInit();
        this.createRecipeVewModelInit();
        this.recipeSearchViewInit();
        this.createRecipeShowSingleViewModel();
        this.createRecipeVideoViwewModel();

        /**
         * Video
         */
        this.createVideoInit();
        this.videoSearchViewInit();
    }

    private void articleGetViewModel() {
        this.magicMapper.from(Article.class)
                .to(GetArticleViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setBody(entity.getBody().replaceAll(Patterns.REGEX_MATCH_HTML_ATTRIBUTES,""));
                    dto.setPicturePath(entity.getPicture() != null ? entity.getPicture().getPath() : "");
                });
    }

    private void articleViewModel() {
        this.magicMapper.from(Article.class)
                .to(ArticleViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setBody(entity.getBody());
                    dto.setPicturePath(entity.getPicture() != null ? entity.getPicture().getPath() : "");
                    dto.setFacebookId(entity.getFacebookId());
                });
    }

    private void createCategoryInit() {

        this.magicMapper.from(CreateCategoryBindingModel.class)
                .to(Category.class)
                .forRule((dto, entity, mapper) -> {
                    entity.setName(dto.getName());
                    MultipartFile file = dto.getUpload();
                    Picture picture = this.savePicture(file, ComponentFolders.DIRECTORY_CATEGORY);
                    if (picture != null) {
                        entity.setPicture(picture);
                    }
                });
    }

    private void viewCategoryInit() {
        this.magicMapper.from(Category.class)
                .to(ListCategoryViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                });
    }

    private void createVideoInit() {
        this.magicMapper.from(CreateVideoPerCategoryBindingModel.class)
                .to(Video.class)
                .forRule((dto, entity, mapper) -> {
                    entity.setName(dto.getName());
                    entity.setLinkId(dto.getLinkId());
                    entity.setCategories(new ArrayList<>(this.categoryService
                            .findAllById(new ArrayList<>(dto.getCategoriesIds()))));
                    entity.setCreatedOn(new Date());
                });
    }

    private void createRecipeInit() {
        this.magicMapper.from(CreateRecipeBindingModel.class)
                .to(Recipe.class)
                .forRule((dto, entity, mapper) -> {
                    entity.setName(dto.getName());
                    entity.setBody(dto.getBody());
                    entity.setCreatedOn(new Date());

                    CreateVideoPerCategoryBindingModel model = new CreateVideoPerCategoryBindingModel();
                    model.setName(dto.getVideoName());
                    model.setLinkId(dto.getVideoURL());
                    List<Long> catLong = this.categoryService.findAllByName(dto.getCategoryNames())
                            .stream().map(Category::getId).collect(Collectors.toList());
                    model.setCategoriesIds(catLong);
                    model.setRecipe(entity);
                    model.setTagNames(dto.getTagNames());
                    Video video = this.videoService.createVideo(model);
                    entity.setVideo(video);
                    MultipartFile file = dto.getFile();
                    Picture picture = this.savePicture(file, ComponentFolders.DIRECTORY_RECIPE);
                    if (picture != null) {
                        entity.setPicture(picture);
                    }
                    entity.setCategories(new ArrayList<>(this.categoryService
                            .findAllByName(dto.getCategoryNames())));
                    entity.setFacebookId(dto.getFacebookId());
                });
    }

    private void createVideoPerCategoryInit() {
        this.magicMapper.from(Video.class)
                .to(VideoViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setPath(entity.getLinkId());
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setRecipeName(entity.getRecipe().getName());
                    dto.setLinkId(entity.getLinkId());
                    dto.setRecipeId(entity.getRecipe().getId());
                });
    }

    private void createRecipeVideoViwewModel() {
        this.magicMapper.from(Recipe.class)
                .to(RecipeVideoViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setVideoPath(entity.getVideo().getLinkId());
                    dto.setVideoId(entity.getVideo().getId());
                    dto.setRecipeName(entity.getName());
                    dto.setRecipeId(entity.getId());
                    dto.setRecipeName(entity.getName());
                    dto.setVideoLinkId(entity.getVideo().getLinkId());
                    dto.setVideoName(entity.getVideo().getName());
                });
    }

    private void createCategoryViewModelInit() {
        this.magicMapper.from(Category.class)
                .to(CategoryViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setName(entity.getName());
                    dto.setPicturePath(entity.getPicture().getPath());
                    dto.setId(entity.getId());
                });
    }

    private void createRecipeVewModelInit() {
        this.magicMapper.from(Recipe.class)
                .to(RecipePerCategoryViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setId(entity.getId());
                    dto.setBody(entity.getBody());
                    dto.setName(entity.getName());
                    dto.setPicturePath(entity.getPicture().getPath());
                });
    }

    private void editRecipeInit() {

        this.magicMapper.from(RecipeEditBindingModel.class)
                .to(Recipe.class)
                .forRule((dto, entity, mapper) -> {
                    Recipe entityDb = this.recipeService.findOneById(dto.getId());
                    entity.setId(dto.getId());
                    if (!dto.getName().trim().isEmpty()) {
                        entity.setName(dto.getName());
                    }
                    MultipartFile file = dto.getFile();
                    Picture picture = this.savePicture(file, ComponentFolders.DIRECTORY_RECIPE);
                    if (picture != null) {
                        entity.setPicture(picture);
                    } else {
                        entity.setPicture(entityDb.getPicture());
                    }
                    entity.setCreatedOn(entityDb.getCreatedOn());
                    entity.setBody(dto.getBody());
                    Video video = entityDb.getVideo();
                    String linkId = dto.getVideoURL().substring(dto.getVideoURL().indexOf("?v=") + 3);
                    if (!linkId.equals(video.getLinkId())) {
                        video.setLinkId(linkId);
                    }
                    if (!video.getName().equals(dto.getVideoName())) {
                        video.setName(dto.getVideoName());
                    }
                    entity.setVideo(video);
                    entity.setFacebookId(dto.getFacebookId());
                    entity.setCategories(new ArrayList<>(this.categoryService
                            .findAllByName(dto.getCategoryNames())));
                });
    }

    private void editRecipeViewModel() {
        this.magicMapper.from(Recipe.class)
                .to(RecipeEditViewModel.class)
                .forRule((entity, dto, magicMapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setPictureName(entity.getPicture().getPath().replaceAll(Patterns.REGEX_PICTURE_PATH,
                            Patterns.REPLACEMENT_PICTURE_PATH));
                    dto.setBody(entity.getBody());
                    dto.setVideoName(entity.getVideo().getName());
                    dto.setVideoURL("https://www.youtube.com/watch?v=" + entity.getVideo().getLinkId());
                    dto.setFacebookId(entity.getFacebookId());
                    dto.setCategoryNames(entity.getCategories()
                            .stream()
                            .map(Category::getName)
                            .collect(Collectors.toList()));
                });
    }

    private void createArticleInit() {
        this.magicMapper.from(CreateArticleBindingModel.class)
                .to(Article.class)
                .forRule((dto, entity, mapper) -> {
                    entity.setName(dto.getName());
                    entity.setBody(dto.getBody()
                            .replaceAll(Patterns.REGEX_IMG_TAG,
                                    Patterns.REPLACEMENT_IMG_TAG));
                    entity.setCreatedOn(new Date());
                    entity.setDeletedOn(null);
                    entity.setFacebookId(dto.getFacebookId());
                    MultipartFile file = dto.getFile();
                    Picture picture = this.savePicture(file, ComponentFolders.DIRECTORY_RECIPE);
                    if (picture != null) {
                        entity.setPicture(picture);
                    }

                });
    }

    private void editArticleInit() {
        this.magicMapper.from(EditArticleBindingModel.class)
                .to(Article.class)
                .forRule((dto, entity, mapper) -> {
                    Article entityDb = this.articleService.findOne(dto.getId());
                    entity.setId(dto.getId());
                    entity.setName(dto.getName());
                    entity.setBody(dto.getBody()
                            .replaceAll(Patterns.REGEX_IMG_TAG,
                                    Patterns.REPLACEMENT_IMG_TAG));
                    entity.setCreatedOn(new Date());
                    entity.setFacebookId(dto.getFacebookId());
                    MultipartFile file = dto.getFile();
                    Picture picture = this.savePicture(file, ComponentFolders.DIRECTORY_RECIPE);
                    if (picture != null) {
                        entity.setPicture(picture);
                    } else {
                        entity.setPicture(entityDb.getPicture());
                    }
                });
    }

    private void editArticleViewModel() {
        this.magicMapper.from(Article.class)
                .to(EditArticleViewModel.class)
                .forRule((entity, dto, magicMapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setFacebookId(entity.getFacebookId());
                });
    }

    private void createArticlePreviewViewModel() {
        this.magicMapper.from(Article.class)
                .to(ArticlePreviewViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setPicturePath(entity.getPicture() != null ? entity.getPicture().getPath() : "");
                    dto.setContent(entity.getBody().replaceAll(REGEX_MATCH_HTML_ATTRIBUTES,""));
                });
    }

    private void editCategoryInit() {

        this.magicMapper.from(EditCategoryBindingModel.class)
                .to(Category.class)
                .forRule((dto, entity, mapper) -> {
                    entity.setId(dto.getId());
                    if (!dto.getName().trim().isEmpty()) {
                        entity.setName(dto.getName());
                    }
                    MultipartFile file = dto.getFile();
                    Picture picture = this.savePicture(file, ComponentFolders.DIRECTORY_CATEGORY);
                    if (picture != null) {
                        entity.setPicture(picture);
                    }
                });
    }

    private void recipeSearchViewInit() {

        this.magicMapper.from(Recipe.class)
                .to(RecipeSearchViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setName(entity.getName());
                    dto.setBody(entity.getBody());
                    dto.setCategories(entity.getCategories()
                            .stream()
                            .map(e -> this.magicMapper.map(e, ListCategoryViewModel.class))
                            .collect(Collectors.toSet()));
                    dto.setCreatedOn(entity.getCreatedOn());
                    dto.setRaiting(entity.getRating());
                    dto.setPicturePath(entity.getPicture().getPath());
                    dto.setId(entity.getId());
                });
    }

    private void videoSearchViewInit() {

        this.magicMapper.from(Video.class)
                .to(VideoSearchViewModel.class)
                .forRule((entity, dto, mapper) -> {
                    dto.setName(entity.getName());
                    dto.setId(entity.getId());
                    dto.setLinkId(entity.getLinkId());
                    dto.setRecipeId(entity.getRecipe().getId());
                    dto.setRecipeName(entity.getRecipe().getName());
                });
    }

    private void editCategoryViewModel() {
        this.magicMapper.from(Category.class)
                .to(EditCategoryViewModel.class)
                .forRule((entity, dto, magicMapper) -> {
                    dto.setId(entity.getId());
                    dto.setName(entity.getName());
                    dto.setPictureName(entity.getPicture().getPath().replaceAll(Patterns.REGEX_PICTURE_PATH,
                            Patterns.REPLACEMENT_PICTURE_PATH));
                });
    }

    private Picture savePicture(MultipartFile file, String pictureType) {
        if (!file.isEmpty()) {
            String filePathDb = "/img/" + pictureType + "/" + file.getOriginalFilename();
            Picture picture = new Picture();
            picture.setPath(filePathDb);
            return this.pictureRepository.save(picture);
        }
        return null;
    }

    private void createRecipeShowSingleViewModel() {
        this.magicMapper.from(Recipe.class)
                .to(RecipeShowSingleViewModel.class)
                .forRule((entity, dto, magicMapper) -> {
                    dto.setName(entity.getName());
                    dto.setBody(entity.getBody());
                    dto.setRecipeId(entity.getId());
                    dto.setPicturePath(entity.getPicture().getPath());
                    dto.setRating(entity.getRating());
                    dto.setVideoLinkId(entity.getVideo().getLinkId());
                    dto.setFacebookId(entity.getFacebookId());
                });
    }
}
