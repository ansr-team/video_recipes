package app.config;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Custom access denied handler. It is used to extend SpringSecurity's access denied handler.
 * Used to navigate to a custom access denied page. It is used in webConfig to replace the default
 * access denied handler.
 */
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private String errorPage;

    /**
     * Instantiates a new Custom access denied handler.
     */
    public CustomAccessDeniedHandler() {
    }

    /**
     * Instantiates a new Custom access denied handler.
     *
     * @param errorPage the error page as action
     */
    public CustomAccessDeniedHandler(String errorPage) {
        this.errorPage = errorPage;
    }

    /**
     * Gets error page.
     *
     * @return the error page as view
     */
    public String getErrorPage() {
        return errorPage;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException)
            throws IOException, ServletException {

        //do some business logic, then redirect to errorPage url
        response.sendRedirect(this.errorPage);
    }
}