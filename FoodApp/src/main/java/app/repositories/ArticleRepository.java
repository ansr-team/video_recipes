package app.repositories;

import app.models.entity.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    Page<Article> findAllByDeletedOnIsNullOrderByCreatedOnDesc(Pageable pageable);

    List<Article> findAllByDeletedOnIsNullOrderByCreatedOnDesc();

    Article findFirstByDeletedOnIsNullOrderByCreatedOnDesc();

    List<Article> findTop12OrderByDeletedOnIsNullOrderByRatingDesc();

    List<Article> findAllByDeletedOnIsNull();

    Page<Article> findAllByDeletedOnIsNull(Pageable pageable);

    Article findOneByIdAndDeletedOnIsNull(Long id);

    long countByDeletedOnIsNull();

    @Query("SELECT MAX(a.id) FROM Article AS a")
    Long getMaxId();

    List<Article> findAllByDeletedOnIsNullAndFacebookIdNotNull();
}
