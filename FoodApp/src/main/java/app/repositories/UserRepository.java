package app.repositories;

import app.models.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByName(String name);

    @Query("SELECT u.accessToken FROM User as u")
    List<String> getAccessToken();

    @Query("SELECT u FROM User as u JOIN u.authorities as a WHERE a.name=\'ADMIN\'")
    List<User> findAllByAuthoritiesAdmin();
}
