package app.repositories;

import app.models.entity.Video;
import app.repositories.searchRepository.video.VideoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoRepository extends JpaRepository<Video, Long>, VideoRepositoryCustom {

    @Query("SELECT COUNT(video.name) FROM Video AS video WHERE video.name=:name")
    int checkNameExistCount(@Param("name") String name);

    @Query("SELECT COUNT(video.linkId) FROM Video AS video WHERE video.linkId=:linkId")
    int checkLinkIdExistCount(@Param("linkId") String linkId);

    List<Video> findTop8ByAndRecipeDeletedOnIsNullOrderByRatingDesc();

    Video findVideoByLinkId(String linkId);

    @Query("SELECT distinct video FROM Video AS video ORDER BY video.id ASC ")
    List<Video> getAllOrderedById();

}
