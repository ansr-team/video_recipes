package app.repositories;

import app.models.entity.Recipe;
import app.repositories.searchRepository.recipe.RecipeRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long>, RecipeRepositoryCustom {

    Page<Recipe> findAllByDeletedOnIsNull(Pageable pageable);

    Recipe findAllByVideo_IdAndDeletedOnIsNull(Long videoId);

    Recipe findAllByNameAndDeletedOnIsNull(String name);

    List<Recipe> findAllByDeletedOnIsNull();

    long countByDeletedOnIsNull();

    Page<Recipe> findAllByDeletedOnIsNullOrderByCreatedOnDesc(Pageable pageable);

    Page<Recipe> findAllByDeletedOnIsNullAndCategories_idOrderByCreatedOnDesc(Long id, Pageable pageable);

    List<Recipe> findAllByDeletedOnIsNullAndFacebookIdNotNull();

    @Query("SELECT MAX(r.id) FROM Recipe AS r")
    Long getMaxId();
}
