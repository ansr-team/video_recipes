package app.repositories.searchRepository.video;

import app.models.entity.Video;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class VideoRepositoryImpl implements VideoRepositoryCustom {

    private final String VIDEO_SEARCH_QUERY = "SELECT DISTINCT video from Video as video " +
            "LEFT OUTER JOIN video.tags as vt " +
            "LEFT OUTER JOIN video.categories as vc " +
            "WHERE video.recipe.deletedOn IS NULL AND (" +
            " video.name LIKE ";

    private final String VIDEO_SEARCH_CATEGORY_QUERY =
            "SELECT DISTINCT video from Video as video " +
            "LEFT OUTER JOIN video.tags as vt " +
            "LEFT OUTER JOIN video.categories as vc " +
            "WHERE video.recipe.deletedOn IS NULL AND vc.name IN :catNames AND (" +
            " video.name LIKE ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<Video> findAllByKeyWords(String... keywords) {
        String queryString = this.VIDEO_SEARCH_QUERY;

        if (keywords.length > 0) {
            queryString = queryString +
                    "'%" + keywords[0] + "%' ";
        }

        List<String> orStatement = new ArrayList<>();

        for (int i = 1; i < keywords.length; i++) {
            orStatement.add("OR video.name LIKE '%" + keywords[i] + "%'");

        }

        queryString = queryString + String.join(" ", orStatement);
        queryString = queryString + " OR vt.name IN :keywords )" + " ORDER BY video." + "name";

        Query query = this.entityManager
                .createQuery(queryString);
        query.setParameter("keywords", Arrays.asList(keywords));

        return Collections.unmodifiableList(query.getResultList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Video> findAllByKeyWords(String order, String... keywords) {
        String queryString = this.VIDEO_SEARCH_QUERY +
                "'%" + keywords[0] + "%' ";

        List<String> orStatement = new ArrayList<>();

        for (String word : keywords) {
            orStatement.add("OR video.name LIKE '%" + word + "%'");
        }

        queryString = queryString + String.join(" ", orStatement);
        queryString = queryString + " OR vt.name IN :keywords )";
        queryString = queryString + " ORDER BY video." + order;

        Query query = this.entityManager
                .createQuery(queryString);
        query.setParameter("keywords", Arrays.asList(keywords));

        return Collections.unmodifiableList(query.getResultList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Video> findAllByKeyWords(String order,List<String> catNames ,String... keywords) {

        String queryString = this.VIDEO_SEARCH_CATEGORY_QUERY +
                "'%" + keywords[0] + "%' ";

        List<String> orStatement = new ArrayList<>();

        for (String word : keywords) {
            orStatement.add("OR video.name LIKE '%" + word + "%'");
        }

        queryString = queryString + String.join(" ", orStatement);
        queryString = queryString + " OR vt.name IN :keywords )";
        queryString = queryString + " ORDER BY video." + "name";

        Query query = this.entityManager
                .createQuery(queryString);
        query.setParameter("keywords", Arrays.asList(keywords));
        query.setParameter("catNames", catNames);

        return Collections.unmodifiableList(query.getResultList());
    }
}
