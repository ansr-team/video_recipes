package app.repositories.searchRepository.video;

import app.models.entity.Video;

import java.util.List;


public interface VideoRepositoryCustom {

    List<Video> findAllByKeyWords(String... keywords);

    List<Video> findAllByKeyWords(String order, String... keywords);

    List<Video> findAllByKeyWords(String order,List<String> catNames ,String... keywords);
}
