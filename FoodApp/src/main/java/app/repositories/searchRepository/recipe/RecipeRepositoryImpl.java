package app.repositories.searchRepository.recipe;

import app.models.entity.Recipe;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collections;
import java.util.List;

public class RecipeRepositoryImpl implements RecipeRepositoryCustom {

    private final String RECIPE_SEARCH_QUERY = "SELECT DISTINCT recipe from Recipe as recipe " +
            "WHERE recipe.deletedOn IS NULL AND recipe.body ";

    private final String RECIPE_CATEGORY_SEARCH_QUERY = "SELECT DISTINCT recipe from Recipe as recipe " +
            " LEFT OUTER JOIN recipe.categories AS rc " +
            "WHERE recipe.deletedOn IS NULL AND rc.deletedOn IS NULL AND rc.name IN :catNames AND ( recipe.body ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<Recipe> findAllByKeyWords(String... keywords) {

        String queryString = "";

        queryString = RECIPE_SEARCH_QUERY + this.fillQueryString(keywords);

        queryString = queryString + " ORDER BY recipe.name";
        Query query = this.entityManager
                .createQuery(queryString);

        return Collections.unmodifiableList(query.getResultList());
    }

    @Override
    public List<Recipe> findAllByKeyWords(String order, String... keywords) {

        String queryString = "";
        if (!order.equals("cat")) {
            queryString = RECIPE_SEARCH_QUERY + this.fillQueryString(keywords) + " ORDER BY recipe." + order;
        } else {
            queryString = RECIPE_CATEGORY_SEARCH_QUERY + this.fillQueryString(keywords) + " ) ORDER BY recipe.name";

        }

        Query query = this.entityManager
                .createQuery(queryString);

        return Collections.unmodifiableList(query.getResultList());
    }

    @Override
    public List<Recipe> findAllByKeyWords(String order, List<String> catNames,String... keywords) {
        Query query;
        String queryString = "";
        if (!order.equals("cat")) {
            queryString = RECIPE_SEARCH_QUERY + this.fillQueryString(keywords) + " ORDER BY recipe." + order;
            query = this.entityManager
                    .createQuery(queryString);
        } else {
            queryString = RECIPE_CATEGORY_SEARCH_QUERY + this.fillQueryString(keywords) + " ) ORDER BY recipe.name";
            query = this.entityManager.createQuery(queryString);
            query.setParameter("catNames",catNames);
        }

        return Collections.unmodifiableList(query.getResultList());
    }

    private String fillQueryString(String[] keywords) {
        String queryString = "";
        if (keywords.length > 0) {
            queryString = queryString + " LIKE '%" + keywords[0] + "%'";
        }

        for (int i = 1; i < keywords.length; i++) {
            queryString = queryString + " OR recipe.body LIKE '%" + keywords[i] + "%' ";
        }
        return queryString;
    }
}
