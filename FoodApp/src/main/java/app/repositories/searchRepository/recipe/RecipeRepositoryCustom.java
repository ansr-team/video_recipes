package app.repositories.searchRepository.recipe;

import app.models.entity.Recipe;

import java.util.List;

public interface RecipeRepositoryCustom {

    List<Recipe> findAllByKeyWords(String... keywords);

    List<Recipe> findAllByKeyWords(String order, String... keywords);

    List<Recipe> findAllByKeyWords(String order, List<String> catNames,String... keywords);
}
