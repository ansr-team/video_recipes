package app.repositories;

import app.models.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    @Query("SELECT COUNT(t)>0 FROM Tag AS t WHERE t.name=:tagName")
    boolean isPresent(@Param("tagName")String tagName);

    Tag findOneByName(String name);

    @Query("SELECT t.name FROM Tag as t")
    List<String> getAllTags();

}
