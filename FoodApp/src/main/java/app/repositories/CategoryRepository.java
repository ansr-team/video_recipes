package app.repositories;

import app.models.entity.Category;
import app.models.entity.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findAllByDeletedOnIsNull();

    List<Category> findByNameInAndDeletedOnIsNull(List<String> names);

    Page<Category> findAllByDeletedOnIsNull(Pageable pageable);

    Category findOneByIdAndDeletedOnIsNull(Long id);

    @Query("SELECT category.name FROM Category as category WHERE category.deletedOn IS NULL")
    List<String> findAllNameByDeletedOnIsNull();

    long countByDeletedOnIsNull();
}
