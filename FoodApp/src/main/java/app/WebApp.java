package app;

import app.config.MagicMapperConfig;
import app.helper.magicMapper.ANSRMagicMapper;
import app.helper.magicMapper.MagicMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Properties;

@SpringBootApplication
@PropertySource(value = {"classpath:app.properties", "classpath:local.properties"})
@EnableScheduling
public class WebApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(WebApp.class, args);
    }

    @Bean
    public MagicMapper magicMapper() {
        MagicMapper magicMapper = new ANSRMagicMapper();
        MagicMapperConfig config = new MagicMapperConfig(magicMapper);
        return magicMapper;
    }

    @Bean
    public MailSender createSender() {
        JavaMailSenderImpl ms = new JavaMailSenderImpl();
        ms.setHost("smtp.gmail.com");
        ms.setPort(587);
        ms.setUsername("foodiehour");
        ms.setPassword("SmartShak3");

        Properties prop = new Properties();
        prop.setProperty("mail.transport.protocol", "smtp");
        prop.setProperty("mail.smtp.auth", "true");
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.debug", "true");

        ms.setJavaMailProperties(prop);

        return ms;
    }

    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    private static Class<WebApp> applicationClass = WebApp.class;
}
