package app.authentication;

import app.constants.AdminData;
import app.models.entity.Authority;
import app.models.entity.User;
import app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static app.constants.SecurityConstants.OAUTH_ACCESS_TOKEN;

@Component
public class AdminInitializer {

    private final UserService userService;

    @Autowired
    public AdminInitializer(UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    private void init() {
        User admin = this.userService.findOneByUsername(AdminData.ADMIN_USERNAME);

        if (admin == null) {
            admin = new User();
            admin.setName(AdminData.ADMIN_USERNAME);
            admin.setAccessToken(OAUTH_ACCESS_TOKEN);
            admin.setPassword(new BCryptPasswordEncoder().encode(AdminData.ADMIN_PASSWORD));
            Authority authority = new Authority();
            authority.setName(AdminData.ADMIN_ROLE);
            admin.getAuthorities().add(authority);
            admin.setHasValidToken(false);
            this.userService.createAdmin(admin);
        }
    }
}