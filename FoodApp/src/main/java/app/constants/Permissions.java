package app.constants;

public class Permissions {

    public static final String PERMISSION_IS_LOGGED = "isAuthenticated()";
    public static final String PERMISSION_IS_ADMIN = "hasAuthority('ADMIN')";
    public static final String PERMISSION_IS_USER = "hasRole('USER')";

}
