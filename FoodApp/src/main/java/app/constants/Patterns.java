package app.constants;

public class Patterns {

//    public static final String REGEX_H_TAG = "<h\\d{1}(?=>)";
    public static final String REGEX_IMG_TAG = "(<img)(?!\\s*alt=\\\"\\w*?\\\"\\s*?class|\\s*class)";
//    public static final String REGEX_P_TAG = "<p(?=\\s*>)";
    public static final String REGEX_PLAIN_TEXT = "<img.*\\/>|<img.*><\\/img>";
    public static final String REGEX_PICTURE_PATH = "([\\/a-zA-Z-]+\\/)";
    public static final String REGEX_MATCH_HTML_ATTRIBUTES = "<\\/*([a-z][a-z0-9]*)\\b[^>]*>(.*?)";

//    public static final String REPLACEMENT_H_TAG = "$0 class=\"wow fadeInRight animated\"";
    public static final String REPLACEMENT_IMG_TAG = "$1 class=\"lazy-loaded\"";
//    public static final String REPLACEMENT_P_TAG = "$0 class=\"wow fadeInLeft animated animated\" data-wow-delay=\".4s\" style=\"visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;\"";
    public static final String REPLACEMENT_PLAIN_TEXT = "";
    public static final String REPLACEMENT_PICTURE_PATH = "";
}
