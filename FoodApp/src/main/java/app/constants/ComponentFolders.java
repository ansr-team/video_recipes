package app.constants;

public class ComponentFolders {

    public static final String DIRECTORY_CATEGORY = "category";
    public static final String DIRECTORY_ARTICLE= "article";
    public static final String DIRECTORY_RECIPE= "recipe";
    public static final String DIRECTORY_PICTURE_UPLOAD = "/img/picture/";

}
