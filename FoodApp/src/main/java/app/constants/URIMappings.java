package app.constants;

public class URIMappings {


    /**
     * Admin
     */
    public static final String URI_ADMIN_LOGIN = "/login";
    public static final String URI_ADMIN_LOGOUT = "/logout";
    public static final String URI_ADMIN_MAIN_VIEW = "/admin";
    public static final String URI_ADMIN_URL = "/admin/**";

    /**
     * Article
     */
    public static final String URI_ARTICLE_CREATE = "/admin/articles/create";
    public static final String URI_ARTICLE_EDIT = "/admin/articles/{id}/edit";
    public static final String URI_ARTICLE_VIEW = "/admin/articles/{id}";
    public static final String URI_ARTICLE_PICTURE_UPLOAD = "/articles/picture/upload";
    public static final String URI_ALL_ARTICLES = "/admin/allArticles";
    public static final String URI_ARTICLES_ALL_PAGEABLE_ADMIN = "/admin/allArticles/pageable";
    public static final String URI_ARTICLE_DELETE = "/admin/articles/{id}/delete";

    /**
     * Blog
     */
    public static final String URI_BLOG = "/blog";
    public static final String URI_BLOG_ARTICLE = "/blog/{id}";
    public static final String URI_BLOG_PAGEABLE = "/blog/pageable";
    public static final String URI_BLOG_ARTICLE_LINK = "https://fodiehour.com/blog/%d";

    /**
     * Category
     */
    public static final String URI_CATEGORY_CREATE = "/admin/categories/create";
    public static final String URI_CATEGORY_VIDEOS_VIEW = "/categories/{categoryId}/videos";
    public static final String URI_CATEGORIES_RECIPES = "/categories/{categoryId}/recipes";
    public static final String URI_CATEGORIES_TOGGLE = "/categories/toggle";
    public static final String URI_ALL_CATEGORIES = "/admin/allCategories";
    public static final String URI_ALL_CATEGORIES_PAGEABLE = "/admin/allCategories/pageable";
    public static final String URI_CATEGORY_DELETE = "/admin/categories/{id}/delete";
    public static final String URI_CATEGORY_EDIT = "/admin/categories/{id}/edit";
    public static final String URI_CATEGORY_NAMES = "/categories/names";

    /**
     * Contacts
     */
    public static final String URI_CONTACTS = "/contacts";

    /**
     * HOME
     */
    public static final String URI_HOME_INDEX = "/";


    /**
     * Recipe
     */
    public static final String URI_RECIPE_SHOW = "/recipes/{id}";
    public static final String URI_RECIPES_ALL_ADMIN = "/admin/allRecipes";
    public static final String URI_RECIPES_ALL_PAGEABLE_ADMIN = "/admin/allRecipes/pageable";
    public static final String URI_RECIPE_CREATE_ADMIN = "/admin/recipes/create";
    public static final String URI_RECIPE_EDIT_ADMIN = "/admin/recipes/{id}/edit";
    public static final String URI_RECIPE_DELETE_ADMIN = "/admin/recipes/{id}/delete";
    public static final String URI_RECIPES_PICTURE_UPLOAD = "/recipes/picture/upload";
    public static final String URI_RECIPES_PER_CATEGORY = "/categories/{id}/recipes/pageable";
    public static final String URI_RECIPE_ARTICLE_LINK = "https://fodiehour.com/recipes/%d";


    /**
     * Search
     */
    public static final String URI_SEARCH = "/theme/search";
    public static final String URI_SEARCH_ORDERED = "/search/ordered";

    /**
     * Video
     */
    public static final String URI_VIDEO_CREATE_COMMENT = "/videos/{videoId}/comment";
    public static final String URI_VIDEO_SINGLE_VIEW = "/videos/{videoId}";
    public static final String URI_VIDEO_UPLOAD = "/videos/upload";
    public static final String URI_ALL_VIDEOS_IDS = "/allVideoIds";
    public static final String URI_VIDEO_RECIPE_NAME = "/video/{id}";
    public static final String URI_VIDEOS_BY_PAGE = "/videos/pageable";
    public static final String URI_VIDEOS = "/videos";

    /**
     * Tag
     */
    public static final String URI_GET_TAGS = "/getTags";

    /**
     * Facebook
     */
    public static final String UPDATE_TOKEN = "/update_token";

    public static final String ACCESS_DENIED = "/error/accessDenied";

    /**
     * Privacy Policy
     */
    public static final String URI_PRIVACY_POLICY = "/privacyPolicy";

    /**
     * About us
     */
    public static final String URI_ABOUT_US= "/about";

}
