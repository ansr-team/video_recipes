package app.services.user;

import app.models.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User findOneByUsername(String username);

    void createAdmin(User admin);

    String getAccessToken();

    List<User> getAdminProfiles();

    User getCurrentAdmin();
}
