package app.services.user;

import app.authentication.CurrentUser;
import app.models.entity.User;
import app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findOneByUsername(String username) {
        return this.userRepository.findOneByName(username);
    }

    @Override
    public void createAdmin(User admin) {
        this.userRepository.saveAndFlush(admin);
    }

    @Override
    public String getAccessToken() {
        return this.userRepository.getAccessToken().get(0);
    }

    @Override
    public List<User> getAdminProfiles() {
        return Collections.unmodifiableList(this.userRepository.findAllByAuthoritiesAdmin());
    }

    @Override
    public User getCurrentAdmin() {
        return ((CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.findOneByUsername(username);
        return new CurrentUser(user);
    }
}
