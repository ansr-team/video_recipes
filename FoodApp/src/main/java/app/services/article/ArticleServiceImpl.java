package app.services.article;

import app.constants.ComponentFolders;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.article.CreateArticleBindingModel;
import app.models.dto.bindingModel.article.EditArticleBindingModel;
import app.models.entity.Article;
import app.repositories.ArticleRepository;
import app.services.file.FileService;
import app.services.file.FileUploadProcess;
import app.services.picture.PictureService;
import app.services.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

    private final MagicMapper mapper;
    private final ArticleRepository articleRepository;
    private final TagService tagService;
    private final PictureService pictureService;
    private final FileService fileService;

    @Autowired
    public ArticleServiceImpl(MagicMapper mapper,
                              ArticleRepository articleRepository,
                              TagService tagService,
                              PictureService pictureService,
                              FileService fileService) {
        this.mapper = mapper;
        this.articleRepository = articleRepository;
        this.tagService = tagService;
        this.pictureService = pictureService;
        this.fileService = fileService;
    }

    @Override
    public Article create(CreateArticleBindingModel model) throws NoSuchFieldException, IllegalAccessException, IOException {
        Article article = this.mapper.map(model, Article.class);
        this.tagService.fillTags(model.getTagNames(), article);
        if (!model.getFile().getOriginalFilename().isEmpty()) {
            MultipartFile file = FileUploadProcess.pictureUploadProcess(model.getFile(),pictureService);
            model.setFile(file);
            this.fileService.upload(file, ComponentFolders.DIRECTORY_RECIPE);
        }
        Article articleMapped = this.mapper.map(model, Article.class);
        this.tagService.fillTags(model.getTagNames(), articleMapped);

        return this.articleRepository.saveAndFlush(article);
    }

    @Override
    public Article edit(EditArticleBindingModel model) {
        Article article = this.mapper.map(model, Article.class);
        return this.articleRepository.save(article);
    }

    @Override
    public Article findOne(Long id) {
        return this.articleRepository.findOneByIdAndDeletedOnIsNull(id);
    }

    @Override
    public Page<Article> getAllOrderByDate(Pageable pageable) {
        return this.articleRepository.findAllByDeletedOnIsNullOrderByCreatedOnDesc(pageable);
    }

    @Override
    public List<Article> getAllOrderByDate() {
        return this.articleRepository.findAllByDeletedOnIsNullOrderByCreatedOnDesc();
    }

    public List<Article> findAll() {
        return this.articleRepository.findAllByDeletedOnIsNull();
    }

    @Override
    public Page<Article> findAll(Pageable pageable) {
        return this.articleRepository.findAllByDeletedOnIsNull(pageable);
    }

    public Long getCount() {
        return this.articleRepository.countByDeletedOnIsNull();
    }

    @Override
    public Article findMostRecent() {

        return this.articleRepository.findFirstByDeletedOnIsNullOrderByCreatedOnDesc();
    }

    @Override
    public List<Article> getTopTwelveByRank() {
        return this.articleRepository.findTop12OrderByDeletedOnIsNullOrderByRatingDesc();
    }

    @Override
    public void delete(Long articleId) {
        Article article = this.articleRepository.findOneByIdAndDeletedOnIsNull(articleId);
        article.setDeletedOn(new Date());
        article.setFacebookId(null);
        this.articleRepository.saveAndFlush(article);
    }

    @Override
    public void updateFacebookRating(Article article) {
        this.articleRepository.saveAndFlush(article);
    }

    @Override
    public Long getLastId() {
        Long lastId = null;
        if ((lastId = this.articleRepository.getMaxId()) != null) {
            return lastId;
        }
        return 1L;
    }

    @Override
    public List<Article> findAllFromFacebook() {
        return this.articleRepository.findAllByDeletedOnIsNullAndFacebookIdNotNull();
    }

}
