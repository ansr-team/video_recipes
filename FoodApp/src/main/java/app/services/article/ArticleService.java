package app.services.article;

import app.models.dto.bindingModel.article.CreateArticleBindingModel;
import app.models.dto.bindingModel.article.EditArticleBindingModel;
import app.models.entity.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface ArticleService {

    Article create(CreateArticleBindingModel model) throws NoSuchFieldException, IllegalAccessException, IOException;

    Article edit(EditArticleBindingModel model);

    Article findOne(Long id);

    Page<Article> getAllOrderByDate(Pageable pageable);

    List<Article> getAllOrderByDate();

    List<Article> findAll();

    Page<Article> findAll(Pageable pageable);

    Long getCount();

    Article findMostRecent();

    List<Article> getTopTwelveByRank();

    void delete(Long articleId);

    void updateFacebookRating(Article article);

    Long getLastId();

    List<Article> findAllFromFacebook();
}
