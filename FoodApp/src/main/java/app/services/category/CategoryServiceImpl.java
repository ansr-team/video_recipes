package app.services.category;

import app.constants.ComponentFolders;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.category.CreateCategoryBindingModel;
import app.models.dto.bindingModel.category.EditCategoryBindingModel;
import app.models.entity.Category;
import app.repositories.CategoryRepository;
import app.services.file.FileService;
import app.services.file.FileUploadProcess;
import app.services.picture.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private static final String PATH_STATIC = "static/";
    private static final String DIRECTORY_IMAGE = "img/";

    private final CategoryRepository categoryRepository;
    private final FileService fileService;
    private final MagicMapper magicMapper;
    private final PictureService pictureService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository,
                               MagicMapper magicMapper,
                               FileService fileService,
                               PictureService pictureService) {
        this.categoryRepository = categoryRepository;
        this.magicMapper = magicMapper;
        this.fileService = fileService;
        this.pictureService = pictureService;
    }

    @Override
    public Category findOneById(Long categoryId) {
        return this.categoryRepository.findOneByIdAndDeletedOnIsNull(categoryId);
    }

    @Override
    public Category create(CreateCategoryBindingModel model) throws IOException {

        if (!model.getUpload().getOriginalFilename().isEmpty()) {
            MultipartFile file = FileUploadProcess.pictureUploadProcess(model.getUpload(),pictureService);
            model.setUpload(file);
            this.fileService.upload(file, ComponentFolders.DIRECTORY_CATEGORY);
        }

        return this.categoryRepository.saveAndFlush(this.magicMapper.map(model, Category.class));
    }

    @Override
    public List<Category> findAllById(List<Long> categoriesIds) {
        return this.categoryRepository.findAll(categoriesIds);
    }

    @Override
    public List<Category> findAll() {
        List<Category> result = this.categoryRepository.findAllByDeletedOnIsNull();
        return result;
    }


    @Override
    public Long recipesCount(Long categoryId) {
        return Long.parseLong(String.valueOf(this.findOneById(categoryId).getRecipes().size()));
    }

    @Override
    public Page<Category> findAll(Pageable pageable) {
        return this.categoryRepository.findAllByDeletedOnIsNull(pageable);
    }

    @Override
    public Long getCount() {
        return this.categoryRepository.countByDeletedOnIsNull();
    }

    @Override
    public void edit(Long categoryId, EditCategoryBindingModel model) throws IOException {
        Category category = this.categoryRepository.findOneByIdAndDeletedOnIsNull(categoryId);

        if (!model.getFile().getOriginalFilename().isEmpty()) {
            MultipartFile file = FileUploadProcess.pictureUploadProcess(model.getFile(),pictureService);
            model.setFile(file);
            this.fileService.upload(file, ComponentFolders.DIRECTORY_CATEGORY);
            this.categoryRepository.save(this.magicMapper.map(model, Category.class));
        } else {
            category.setName(model.getName());
            this.categoryRepository.save(category);
        }
    }

    @Override
    public boolean deleteCategory(Long categoryId) {

        Category category = this.categoryRepository.findOneByIdAndDeletedOnIsNull(categoryId);
        if (category != null) {
            category.setDeletedOn(new Date());
            this.categoryRepository.save(category);
            return true;
        }
        return false;
    }

    @Override
    public List<Category> findAllByName(List<String> names) {

        List<Category> results = this.categoryRepository
                .findByNameInAndDeletedOnIsNull(names.isEmpty() ? Arrays.asList(""):names);
        return results;
    }

    @Override
    public List<String> getAllCategoriesNames() {
        return this.categoryRepository.findAllNameByDeletedOnIsNull();
    }

}
