package app.services.category;

import app.models.dto.bindingModel.category.CreateCategoryBindingModel;
import app.models.dto.bindingModel.category.EditCategoryBindingModel;
import app.models.entity.Category;
import app.models.entity.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface CategoryService {

    Category findOneById(Long categoryId);

    Category create(CreateCategoryBindingModel model) throws IOException;

    List<Category> findAllById(List<Long> categoriesIds);

    List<Category> findAll();

    Long recipesCount(Long categoryId);

    Page<Category> findAll(Pageable pageable);

    Long getCount();

    void edit(Long categoryId, EditCategoryBindingModel model) throws IOException;

    boolean deleteCategory(Long categoryId);

    List<Category> findAllByName(List<String> name);

    List<String> getAllCategoriesNames();
}
