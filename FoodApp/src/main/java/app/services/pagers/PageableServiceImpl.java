package app.services.pagers;

import app.helper.magicMapper.MagicMapper;
import app.models.dto.viewModel.PageableViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PageableServiceImpl implements PageableService {

    private final MagicMapper mapper;

    @Autowired
    public PageableServiceImpl(MagicMapper mapper) {
        this.mapper = mapper;
    }

    public <T, V extends PageableViewModel<V>> V
    getPageableViewModel(Pageable pageable,
                         Collection<T> data,
                         Class<V> viewModelType,
                         Long count,
                         int currentPage) throws IllegalAccessException,
            InstantiationException {

        List<V> mappedData = data.stream()
                .map(e ->
                        this.mapper
                                .map(e, viewModelType))
                .collect(Collectors.toList());

        Page<V> page = new PageImpl<>(mappedData);

        double itemsPerPage = pageable.getPageSize();
        int lastPage = (int)Math.ceil(count / itemsPerPage);

        List<Integer> renderNextPages = new ArrayList<>();
        List<Integer> renderPreviousPages = new ArrayList<>();

        if (currentPage != 0)
        renderPreviousPages.add(0);


        for (int i = 1; i < currentPage; i++) {
            renderPreviousPages.add(i);
        }

        for (int i = 1; i + currentPage < lastPage; i++) {
            renderNextPages.add(i + currentPage);
        }

        V viewModel = viewModelType.newInstance();
        viewModel.setCurrentPage(currentPage);
        viewModel.setNextPages(renderNextPages);
        viewModel.setPreviousPages(renderPreviousPages);
        viewModel.setCount(count);
        viewModel.setPage(page);
        return viewModel;
    }
}
