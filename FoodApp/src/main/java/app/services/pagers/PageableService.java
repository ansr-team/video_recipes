package app.services.pagers;


import app.models.dto.viewModel.PageableViewModel;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

public interface PageableService {

    <T, V extends PageableViewModel<V>> V getPageableViewModel(Pageable pageable,
                           Collection<T> data,
                           Class<V> viewModelType,
                           Long count,
                           int currentPage) throws IllegalAccessException, InstantiationException;

}
