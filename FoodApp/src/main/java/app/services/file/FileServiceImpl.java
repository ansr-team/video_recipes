package app.services.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
@Transactional
public class FileServiceImpl implements FileService {

    private static final String PATH_STATIC = "static/";
    private static final String DIRECTORY_IMAGE = "img/";

    private final Environment env;

    @Autowired
    public FileServiceImpl(Environment env) {
        this.env = env;
    }

    @Override
    public String upload(MultipartFile file, String directory) throws IOException {
        String appPath = this.env.getProperty("project.dir");
        String imagePath = String.format("%s%s%s%s/", appPath, PATH_STATIC, DIRECTORY_IMAGE, directory);
        String filePath = imagePath + file.getOriginalFilename();
        file.transferTo(new File(filePath));
        String contextPath = this.env.getProperty("server.contextPath");

        return String.format("%s/%s%s/%s", contextPath, DIRECTORY_IMAGE, directory, file.getOriginalFilename());
    }
}
