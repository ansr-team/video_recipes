package app.services.file;

import app.models.entity.Picture;
import app.services.picture.PictureService;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static app.constants.ComponentFolders.DIRECTORY_PICTURE_UPLOAD;

@Component
public final class FileUploadProcess {

    public static MultipartFile pictureUploadProcess(MultipartFile file, PictureService service) {
        MultipartFile multipartFile = file;
        String fileName = multipartFile.getOriginalFilename();

        Picture picture = service.findOneByPath(DIRECTORY_PICTURE_UPLOAD + fileName);

        if (picture != null) {
            String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
            String currentFileName = fileName.substring(0, fileName.indexOf('.'));
            fileName = currentFileName + System.nanoTime() + fileExtension;

            String finalFileName = fileName;
            MultipartFile finalFile = multipartFile;
            MultipartFile renamedFile = new MultipartFile() {
                @Override
                public String getName() {
                    return finalFileName;
                }

                @Override
                public String getOriginalFilename() {
                    return finalFileName;
                }

                @Override
                public String getContentType() {
                    return finalFile.getContentType();
                }

                @Override
                public boolean isEmpty() {
                    return finalFile.isEmpty();
                }

                @Override
                public long getSize() {
                    return finalFile.getSize();
                }

                @Override
                public byte[] getBytes() throws IOException {
                    return finalFile.getBytes();
                }

                @Override
                public InputStream getInputStream() throws IOException {
                    return finalFile.getInputStream();
                }

                @Override
                public void transferTo(File dest) throws IOException, IllegalStateException {
                    finalFile.transferTo(dest);
                }

            };
            multipartFile = renamedFile;
        }

        return multipartFile;
    }
}
