package app.services.video;

import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.video.CreateVideoPerCategoryBindingModel;
import app.models.entity.Category;
import app.models.entity.Video;
import app.repositories.VideoRepository;
import app.services.category.CategoryService;
import app.services.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class VideoServiceImpl implements VideoService {

    private final VideoRepository videoRepository;
    private final CategoryService categoryService;
    private final MagicMapper mapper;
    private final TagService tagService;

    @Autowired
    public VideoServiceImpl(VideoRepository videoRepository,
                            CategoryService categoryService,
                            MagicMapper mapper,
                            TagService tagService) {
        this.videoRepository = videoRepository;
        this.categoryService = categoryService;
        this.mapper = mapper;
        this.tagService = tagService;
    }

    @Override
    public Set<Video> findAll() {
        return Collections.unmodifiableSet(new HashSet<>(this.videoRepository.findAll()));
    }

    @Override
    public Set<Video> findAllByCategory(Long categoryId) {
        Category category = this.categoryService.findOneById(categoryId);
        return Collections.unmodifiableSet(category.getVideos());
    }

    @Override
    public Video findOneById(Long videoId) {
        return this.videoRepository.findOne(videoId);
    }

    @Override
    public Video createVideo(CreateVideoPerCategoryBindingModel model) throws NoSuchFieldException, IllegalAccessException {
        Video video = new Video();
        video.setName(model.getName());
        String youtubeURL = model.getLinkId();
        String linkId = youtubeURL.substring(youtubeURL.indexOf("?v=") + 3);
        video.setLinkId(linkId);
        video.setCategories(this.categoryService.findAllById((model.getCategoriesIds())));
        video.setCreatedOn(new Date());
        video.setRecipe(model.getRecipe());

        this.tagService.fillTags(model.getTagNames(), video);
        this.videoRepository.saveAndFlush(video);
        return video;
    }

    @Override
    public int getNameExistsCount(String videoName) {
        return this.videoRepository.checkNameExistCount(videoName);
    }

    @Override
    public int getLinkIdExistCount(String linkId) {
        return this.videoRepository.checkLinkIdExistCount(linkId);
    }

    @Override
    public List<Video> getTopThreeVideos() {
        return this.videoRepository.findTop8ByAndRecipeDeletedOnIsNullOrderByRatingDesc();
    }

    @Override
    public Video getVideoByLinkId(String linkId) {
        return this.videoRepository.findVideoByLinkId(linkId);
    }

    @Override
    public List<Video> search(String... keyWords) {
        return this.videoRepository.findAllByKeyWords(keyWords);
    }

    @Override
    public List<Video> search(String order, String... keyWords) {
        return this.videoRepository.findAllByKeyWords(order, keyWords);
    }

    @Override
    public List<Video> search(String order, List<String> catNames, String... keyWords) {
        return this.videoRepository.findAllByKeyWords(order,catNames,keyWords);
    }

    @Override
    public List<Video> getAllOrdered() {
        return this.videoRepository.getAllOrderedById();
    }
}
