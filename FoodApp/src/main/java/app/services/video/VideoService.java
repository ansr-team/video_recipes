package app.services.video;

import app.models.dto.bindingModel.video.CreateVideoPerCategoryBindingModel;
import app.models.entity.Video;

import java.util.List;
import java.util.Set;

public interface VideoService {

    Set<Video> findAll();

    Set<Video> findAllByCategory(Long categoryId);

    Video findOneById(Long videoId);

    Video createVideo(CreateVideoPerCategoryBindingModel model) throws NoSuchFieldException, IllegalAccessException;

    int getNameExistsCount(String videoName);

    int getLinkIdExistCount(String linkId);

    List<Video> getTopThreeVideos();

    Video getVideoByLinkId(String linkId);

    List<Video> search(String... keyWords);

    List<Video> search(String order, String... keyWords);

    List<Video> search(String order, List<String> catNames, String... keyWords);

    List<Video> getAllOrdered();

}
