package app.services.recipe;


import app.constants.ComponentFolders;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.recipe.CreateRecipeBindingModel;
import app.models.dto.bindingModel.recipe.RecipeEditBindingModel;
import app.models.entity.Recipe;
import app.repositories.RecipeRepository;
import app.services.category.CategoryService;
import app.services.file.FileService;
import app.services.file.FileUploadProcess;
import app.services.picture.PictureService;
import app.services.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {

    private final MagicMapper magicMapper;
    private final RecipeRepository recipeRepository;
    private final PictureService pictureService;
    private final FileService fileService;
    private final TagService tagService;

    @Autowired
    public RecipeServiceImpl(MagicMapper magicMapper,
                             RecipeRepository recipeRepository,
                             PictureService pictureService,
                             FileService fileService,
                             TagService tagService,
                             CategoryService categoryService) {
        this.magicMapper = magicMapper;
        this.recipeRepository = recipeRepository;
        this.pictureService = pictureService;
        this.fileService = fileService;
        this.tagService = tagService;
    }

    @Override
    public Recipe create(CreateRecipeBindingModel model) throws IOException, NoSuchFieldException, IllegalAccessException {
        if (!model.getFile().getOriginalFilename().isEmpty()) {
            MultipartFile file = FileUploadProcess.pictureUploadProcess(model.getFile(),pictureService);
            model.setFile(file);
            this.fileService.upload(file, ComponentFolders.DIRECTORY_RECIPE);
        }
        Recipe recipe = this.magicMapper.map(model, Recipe.class);
        this.tagService.fillTags(model.getTagNames(), recipe);

        return this.recipeRepository.saveAndFlush(recipe);
    }

    @Override
    public Recipe create(RecipeEditBindingModel model) {
        return this.recipeRepository.saveAndFlush(this.magicMapper.map(model, Recipe.class));
    }

    @Override
    public Recipe findOneById(Long recipeId) {
        return this.recipeRepository.findOne(recipeId);
    }

    @Override
    public List<Recipe> findAll() {
        return this.recipeRepository.findAllByDeletedOnIsNull();
    }

    @Override
    public Page<Recipe> findAll(Pageable pageable) {
        return this.recipeRepository.findAllByDeletedOnIsNull(pageable);
    }

    @Override
    public List<Recipe> findAllByKeyWords(String... keyWords) {
        return this.recipeRepository.findAllByKeyWords(keyWords);
    }

    @Override
    public List<Recipe> findAllByKeyWords(String order, String... keyWords) {
        return this.recipeRepository.findAllByKeyWords(order, keyWords);
    }

    @Override
    public List<Recipe> findAllByKeyWords(String order, List<String> catNames, String... keyWords) {
        return recipeRepository.findAllByKeyWords(order,catNames,keyWords);
    }

    @Override
    public Long getCount() {
        return this.recipeRepository.countByDeletedOnIsNull();
    }

    @Override
    public void edit(Long recipeId, RecipeEditBindingModel model) throws IOException {
        Recipe recipe = this.recipeRepository.findOne(recipeId);

        if (!model.getFile().getOriginalFilename().isEmpty()) {
            MultipartFile file = FileUploadProcess.pictureUploadProcess(model.getFile(),pictureService);
            model.setFile(file);
            this.fileService.upload(file, ComponentFolders.DIRECTORY_RECIPE);
            Recipe updatedRecipe = this.magicMapper.map(model, Recipe.class);
            updatedRecipe.getVideo().setCategories(updatedRecipe.getCategories());
            this.recipeRepository.save(updatedRecipe);
        } else {
            Recipe updatedRecipe = this.magicMapper.map(model,Recipe.class);
            updatedRecipe.getVideo().setCategories(updatedRecipe.getCategories());
            this.recipeRepository.save(updatedRecipe);
        }
    }

    @Override
    public boolean delete(Long recipeId) {
        Recipe recipe = this.recipeRepository.findOne(recipeId);
        if (recipe != null) {
            recipe.setDeletedOn(new Date());
            recipe.setFacebookId(null);
            this.recipeRepository.save(recipe);
            return true;
        }
        return false;
    }

    @Override
    public Recipe getByVideoId(Long id) {
        return this.recipeRepository.findAllByVideo_IdAndDeletedOnIsNull(id);
    }

    @Override
    public Recipe getByName(String name) {
        return this.recipeRepository.findAllByNameAndDeletedOnIsNull(name);
    }

    @Override
    public Page<Recipe> findAllByDate(Pageable pageable) {
        return this.recipeRepository.findAllByDeletedOnIsNullOrderByCreatedOnDesc(pageable);
    }

    @Override
    public Page<Recipe> findAllByCategoryId(Long id, Pageable pageable) {
        return this.recipeRepository.findAllByDeletedOnIsNullAndCategories_idOrderByCreatedOnDesc(id, pageable);
    }
    @Override
    public void updateFacebookRating(Recipe recipe) {
        this.recipeRepository.saveAndFlush(recipe);
    }

    @Override
    public List<Recipe> findAllFromFacebook() {
        return this.recipeRepository.findAllByDeletedOnIsNullAndFacebookIdNotNull();
    }

    @Override
    public Long getLastId() {
        Long lastId = null;
        if((lastId = this.recipeRepository.getMaxId()) != null) {
            return lastId;
        }
        return 1L;
    }
}
