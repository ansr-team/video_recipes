package app.services.recipe;

import app.models.dto.bindingModel.recipe.CreateRecipeBindingModel;
import app.models.dto.bindingModel.recipe.RecipeEditBindingModel;
import app.models.entity.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface RecipeService {

    Recipe create(CreateRecipeBindingModel model) throws IOException, NoSuchFieldException, IllegalAccessException;

    Recipe create(RecipeEditBindingModel model);

    Recipe findOneById(Long recipeId);

    List<Recipe> findAll();

    Page<Recipe> findAll(Pageable pageable);

    List<Recipe> findAllByKeyWords(String... keyWords);

    List<Recipe> findAllByKeyWords(String order, String... keyWords);

    List<Recipe> findAllByKeyWords(String order,List<String> catNames ,String... keyWords);

    Long getCount();

    void edit(Long recipeId, RecipeEditBindingModel model) throws IOException;

    boolean delete(Long recipeId);

    Recipe getByVideoId(Long id);

    Recipe getByName(String name);

    Page<Recipe> findAllByDate(Pageable pageable);

    Page<Recipe> findAllByCategoryId(Long id, Pageable pageable);

    void updateFacebookRating(Recipe recipe);

    List<Recipe> findAllFromFacebook();

    Long getLastId();
}
