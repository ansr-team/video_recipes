package app.services.tag;

import app.models.entity.Tag;
import app.models.entity.interfaces.Tagable;

import java.util.List;

public interface TagService {

    Tag createTag(String tagName);

    List<String> allAvailableTags();

    Tag findOneByName(String name);

    boolean exist(String tagName);

    void fillTags(List<String> tagNames, Tagable tagable) throws NoSuchFieldException, IllegalAccessException;

}
