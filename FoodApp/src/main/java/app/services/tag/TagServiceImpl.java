package app.services.tag;

import app.models.entity.Tag;
import app.models.entity.interfaces.Tagable;
import app.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag createTag(String tagName) {
        Tag tag = new Tag();
        tag.setName(tagName);
        return this.tagRepository.save(tag);
    }

    @Override
    public List<String> allAvailableTags() {
        return Collections.unmodifiableList(this.tagRepository.getAllTags());
    }

    @Override
    public Tag findOneByName(String name) {
        return this.tagRepository.findOneByName(name);
    }

    @Override
    public boolean exist(String tagName) {
        return this.tagRepository.isPresent(tagName);
    }

    @Override
    public void fillTags(List<String> tagNames, Tagable tagable) throws NoSuchFieldException, IllegalAccessException {

        for (String tagName : tagNames) {

            Tag tag = this.findOneByName(tagName);
            if (tag != null) {
                tagable.getTags().add(tag);
            } else {
                tag = this.createTag(tagName);
                tagable.getTags().add(tag);
            }
        }
    }
}

