package app.services.contacts;


import app.models.dto.bindingModel.contacts.MailBindingModel;

public interface EmailService {

    void readyToSendEmail(MailBindingModel model);

}