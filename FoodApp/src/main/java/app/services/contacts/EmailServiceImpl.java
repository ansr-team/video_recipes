package app.services.contacts;

        import app.models.dto.bindingModel.contacts.MailBindingModel;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.context.ConfigurableApplicationContext;
        import org.springframework.context.support.ClassPathXmlApplicationContext;
        import org.springframework.mail.MailSender;
        import org.springframework.mail.SimpleMailMessage;
        import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private MailSender mailSender; // MailSender interface defines a strategy
    // for sending simple mails

    public void readyToSendEmail(MailBindingModel model) {

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(model.getFromEmail());
        mailMessage.setTo("plamen.koynov90@gmail.com");
        mailMessage.setSubject("Email send from foodiehour");
        mailMessage.setText(model.getMessage());
        this.mailSender.send(mailMessage);

    }
}