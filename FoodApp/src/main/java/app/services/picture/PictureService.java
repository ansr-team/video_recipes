package app.services.picture;

import app.models.entity.Picture;

public interface PictureService {

    Picture findOne(Long id);
    Picture findOneByPath(String path);
    Picture save(String path);

}
