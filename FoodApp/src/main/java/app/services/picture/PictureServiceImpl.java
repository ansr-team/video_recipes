package app.services.picture;

import app.models.entity.Picture;
import app.repositories.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PictureServiceImpl implements PictureService {

    private final PictureRepository pictureRepository;

    @Autowired
    public PictureServiceImpl(PictureRepository pictureRepository) {
        this.pictureRepository = pictureRepository;
    }

    @Override
    public Picture findOne(Long id) {
       return this.pictureRepository.findOne(id);
    }

    @Override
    public Picture findOneByPath(String path) {
        return this.pictureRepository.findOneByPath(path);
    }

    @Override
    public Picture save(String path) {
        Picture picture = new Picture();
        picture.setPath(path);

        return this.pictureRepository.saveAndFlush(picture);
    }
}
