package app.controllers;


import app.constants.URIMappings;
import app.models.dto.bindingModel.contacts.MailBindingModel;
import app.services.contacts.EmailService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

import static app.constants.SecurityConstants.CAPTCHA_SECRET;
import static app.constants.SecurityConstants.CAPTCHA_VERIFY_URL;

@Controller
public class ContactsController extends AbstractController {

    @Autowired
    private EmailService emailService;

    @GetMapping(URIMappings.URI_CONTACTS)
    public String contacts() {

        return view();

    }

    @PostMapping(URIMappings.URI_CONTACTS)
    public String sendEMail(MailBindingModel model,HttpServletRequest request) {

        if (this.verifyCaptcha(request)){
            this.emailService.readyToSendEmail(model);
            return redirect(URIMappings.URI_HOME_INDEX);
        }

        return redirect(URIMappings.ACCESS_DENIED);
    }

    private boolean verifyCaptcha(HttpServletRequest request){

        String recaptcha = request.getParameter("g-recaptcha-response");

        if (recaptcha == null || "".equals(recaptcha)) {
            return false;
        }

        try{
            URL sendUrl = new URL(CAPTCHA_VERIFY_URL);
            HttpsURLConnection connection = (HttpsURLConnection) sendUrl.openConnection();

            connection.setRequestMethod("POST");
            String postParams = "secret=" + CAPTCHA_SECRET + "&response="
                    + recaptcha;
            connection.setDoOutput(true);

            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.writeBytes(postParams);
            writer.flush();
            writer.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));

            String inputLine = null;
            StringBuilder response = new StringBuilder();

            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();

            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(String.valueOf(response)).getAsJsonObject();
            return object.get("success").getAsBoolean();

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
