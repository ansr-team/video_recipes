package app.controllers;

import app.constants.ComponentFolders;
import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.file.UploadFileBindingModel;
import app.models.dto.viewModel.article.GetArticleViewModel;
import app.models.entity.Article;
import app.services.article.ArticleService;
import app.services.file.FileService;
import app.services.picture.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class ArticleController extends AbstractController {

    private final FileService fileService;
    private final PictureService pictureService;
    private final ArticleService articleService;
    private final MagicMapper mapper;

    @Autowired
    public ArticleController(FileService fileService,
                             PictureService pictureService,
                             ArticleService articleService,
                             MagicMapper mapper) {
        this.fileService = fileService;
        this.pictureService = pictureService;
        this.articleService = articleService;
        this.mapper = mapper;
    }

    @GetMapping(URIMappings.URI_ARTICLE_VIEW)
    public String one(@PathVariable Long id, Model model) {
        Article article = this.articleService.findOne(id);
        GetArticleViewModel viewModel = this.mapper.map(article, GetArticleViewModel.class);

        return view(viewModel, model);
    }

    @PostMapping(URIMappings.URI_ARTICLE_PICTURE_UPLOAD)
    public @ResponseBody String uploadPicture(UploadFileBindingModel model) throws IOException, InterruptedException {
        String path = this.fileService.upload(model.getUpload(), ComponentFolders.DIRECTORY_ARTICLE);
        this.pictureService.save(path);

        return "<script>window.parent.CKEDITOR.tools.callFunction('1','"+path+"');</script>";
    }

}
