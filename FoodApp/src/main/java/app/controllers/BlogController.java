package app.controllers;

import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.viewModel.article.ArticlePreviewViewModel;
import app.models.dto.viewModel.article.ArticleViewModel;
import app.models.dto.viewModel.article.GetArticleViewModel;
import app.models.dto.viewModel.blog.BlogViewModel;
import app.models.entity.Article;
import app.services.article.ArticleService;
import app.services.pagers.PageableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BlogController extends AbstractController {

    private static final int ARTICLES_ON_PAGE = 9;

    private final ArticleService articleService;
    private final MagicMapper magicMapper;
    private final PageableService pageableService;

    @Autowired
    public BlogController(ArticleService articleService,
                          MagicMapper magicMapper,
                          PageableService pageableService) {
        this.articleService = articleService;
        this.magicMapper = magicMapper;
        this.pageableService = pageableService;
    }


    @GetMapping(URIMappings.URI_BLOG)
    public String blog(Model model, @PageableDefault(ARTICLES_ON_PAGE) Pageable pageable) {

        BlogViewModel viewModel = new BlogViewModel();
        Article article = this.articleService.findMostRecent();
        ArticlePreviewViewModel mostRecent = null;
        if (article != null) {
            mostRecent = this.magicMapper.map(article, ArticlePreviewViewModel.class);
        }
        viewModel.setMostRecent(mostRecent);
        return view(viewModel, model);
    }

    @GetMapping(URIMappings.URI_BLOG_ARTICLE)
    public String article(Model model,
                          @PathVariable("id") Long articleId) {

        ArticleViewModel viewModel = this.magicMapper
                .map(this.articleService.findOne(articleId),
                        ArticleViewModel.class);

        return view(viewModel, model);
    }

    @GetMapping(URIMappings.URI_BLOG_PAGEABLE)
    public String blogByPage(@PageableDefault(ARTICLES_ON_PAGE) Pageable pageable, Model model, HttpServletRequest request) throws InstantiationException, IllegalAccessException {
        GetArticleViewModel viewModel =
                this.pageableService.getPageableViewModel(
                        pageable,
                        this.articleService.getAllOrderByDate(pageable).getContent(),
                        GetArticleViewModel.class,
                        this.articleService.getCount(),
                        Integer.parseInt(request.getParameter("page"))
                );


        return view(viewModel, model);
    }
}
