package app.controllers;

import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.comment.CreateVideoCommentBindingModel;
import app.models.dto.bindingModel.video.CreateVideoPerCategoryBindingModel;
import app.models.dto.viewModel.category.ListCategoryViewModel;
import app.models.dto.viewModel.comment.VideoCommentViewModel;
import app.models.dto.viewModel.recipe.RecipeVideoViewModel;
import app.models.dto.viewModel.video.ShowSingleVideoViewModel;
import app.models.dto.viewModel.video.VideoViewModel;
import app.models.entity.Recipe;
import app.models.entity.Video;
import app.services.category.CategoryService;
import app.services.pagers.PageableService;
import app.services.recipe.RecipeService;
import app.services.video.VideoService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class VideoController extends AbstractController {

    private static final int ARTICLES_ON_PAGE = 9;
    private final CategoryService categoryService;
    private final VideoService videoService;
    private final MagicMapper magicMapper;
    private final RecipeService recipeService;
    private final PageableService pageableService;


    @Autowired
    public VideoController(CategoryService categoryService,
                           VideoService videoService,
                           MagicMapper mapper,
                           RecipeService recipeService, PageableService pageableService) {

        this.categoryService = categoryService;
        this.videoService = videoService;
        this.magicMapper = mapper;
        this.recipeService = recipeService;
        this.pageableService = pageableService;
    }

    @GetMapping(value = URIMappings.URI_VIDEO_SINGLE_VIEW)
    public String video(Model model, @PathVariable long videoId) {
        Video current = this.videoService.findOneById(videoId);
        ShowSingleVideoViewModel viewModel = new ShowSingleVideoViewModel();
        Set<VideoCommentViewModel> comments = new HashSet<>();
        viewModel.setId(current.getId());
        viewModel.setPath(current.getLinkId());
        viewModel.setComments(comments);
        return view(viewModel, model);
    }

    @PostMapping(value = URIMappings.URI_VIDEO_CREATE_COMMENT)
    public
    @ResponseBody
    String addVideoComment(CreateVideoCommentBindingModel model, @PathVariable long videoId) {

        return new Gson().toJson("comment");
    }

    @GetMapping(value = URIMappings.URI_VIDEO_UPLOAD)
    public String upload(Model model) {
        Set<ListCategoryViewModel> categories = this.createListCategoryViewModels();

        return view(categories, model, new CreateVideoPerCategoryBindingModel());
    }

    @PostMapping(value = URIMappings.URI_VIDEO_UPLOAD)
    public String uploadProcess(@ModelAttribute("bindingModel") CreateVideoPerCategoryBindingModel bindingModel) throws NoSuchFieldException, IllegalAccessException {

        this.videoService.createVideo(bindingModel);

        return redirect(URIMappings.URI_CATEGORY_VIDEOS_VIEW, 1);
    }

    private Set<ListCategoryViewModel> createListCategoryViewModels() {
        return this.categoryService.findAll().stream()
                .map(category -> this.magicMapper.map(category, ListCategoryViewModel.class))
                .collect(Collectors.toSet());
    }

    @GetMapping(URIMappings.URI_VIDEOS)
    public String videos(Model model, @PageableDefault(ARTICLES_ON_PAGE) Pageable pageable) {

        List<RecipeVideoViewModel> viewModel = new ArrayList<>();

        return view(viewModel, model);

    }

    @GetMapping(URIMappings.URI_VIDEOS_BY_PAGE)
    public String videosByPage(@PageableDefault(ARTICLES_ON_PAGE) Pageable pageable, Model model, HttpServletRequest request) throws InstantiationException, IllegalAccessException {

        Integer page = Integer.parseInt(request.getParameter("page"));
        RecipeVideoViewModel viewModel = this.pageableService
                .getPageableViewModel(
                        pageable,
                        this.recipeService.findAllByDate(pageable).getContent(),
                        RecipeVideoViewModel.class,
                        this.recipeService.getCount(),
                        page);

        return view(viewModel, model);
    }

    @GetMapping(value = URIMappings.URI_ALL_VIDEOS_IDS)
    public
    @ResponseBody
    String getVideoIds() {
        List<VideoViewModel> videoLinkIds = this.videoService.getAllOrdered()
                .stream()
                .map(video -> this.magicMapper.map(video, VideoViewModel.class))
                .collect(Collectors.toList());

        return new Gson().toJson(videoLinkIds);
    }

    @GetMapping(URIMappings.URI_VIDEO_RECIPE_NAME)
    public
    @ResponseBody
    String videoId(@PathVariable("id") Long id) {
        Recipe recipe = this.recipeService.findOneById(id);
        return new Gson().toJson(recipe.getName());
    }

}
