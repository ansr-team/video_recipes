package app.controllers;

import app.constants.URIMappings;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutController extends AbstractController {

    @GetMapping(URIMappings.URI_ABOUT_US)
    public String about() {
        return view();
    }
}
