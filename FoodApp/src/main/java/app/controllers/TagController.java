package app.controllers;

import app.constants.URIMappings;
import app.services.tag.TagService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TagController {

    private final TagService tagservice;

    @Autowired
    public TagController(TagService tagservice) {
        this.tagservice = tagservice;
    }

    @GetMapping(value = URIMappings.URI_GET_TAGS)
    public
    @ResponseBody
    String allTags() {
        return new Gson().toJson(this.tagservice.allAvailableTags());
    }
}
