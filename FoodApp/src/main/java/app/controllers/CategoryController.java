package app.controllers;

import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.viewModel.category.CategoryViewModel;
import app.models.dto.viewModel.category.DropDownCategoryViewModel;
import app.models.dto.viewModel.recipe.RecipePerCategoryViewModel;
import app.models.dto.viewModel.recipe.RecipeAllListedPerCategoryViewModel;
import app.models.dto.viewModel.video.AllVideosPerCategoryViewModel;
import app.models.dto.viewModel.video.VideoViewModel;
import app.models.entity.Category;
import app.models.entity.Video;
import app.services.category.CategoryService;
import app.services.recipe.RecipeService;
import app.services.video.VideoService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class CategoryController extends AbstractController {

    private static final int ARTICLES_ON_PAGE = 9;


    private final MagicMapper magicMapper;
    private final CategoryService categoryService;


    @Autowired
    public CategoryController(MagicMapper mapper,
                              CategoryService categoryService) {
        this.magicMapper = mapper;
        this.categoryService = categoryService;
    }

    @GetMapping(value = URIMappings.URI_CATEGORY_VIDEOS_VIEW)
    public String videos(Model model, @PathVariable long categoryId) {
        Category current = this.categoryService.findOneById(categoryId);
        Set<VideoViewModel> videos = current.getVideos()
                .stream()
                .map(video -> this.magicMapper.map(video, VideoViewModel.class))
                .collect(Collectors.toSet());
        AllVideosPerCategoryViewModel viewModel = new AllVideosPerCategoryViewModel();
        viewModel.setVideos(videos);
        return view(viewModel, model);
    }

    @GetMapping(value = URIMappings.URI_CATEGORIES_RECIPES)
    public String recipes(Model model, @PathVariable long categoryId) {
        Category currentCategory = this.categoryService.findOneById(categoryId);
        List<RecipePerCategoryViewModel> allRecipesPerCategory = currentCategory.getRecipes()
                .stream()
                .filter(recipe -> recipe.getDeletedOn() == null)
                .map(recipe -> this.magicMapper.map(recipe, RecipePerCategoryViewModel.class))
                .collect(Collectors.toList());
        RecipeAllListedPerCategoryViewModel viewModel = new RecipeAllListedPerCategoryViewModel();
        viewModel.setNewestRecipes(allRecipesPerCategory);

        return view(viewModel, model);
    }


    @GetMapping(value = URIMappings.URI_CATEGORIES_TOGGLE)
    public String dropdownPartial(Model model) {

        Pageable pageable = new PageRequest(0, 12);
        List<CategoryViewModel> categories = this.categoryService.findAll(pageable).getContent()
                .stream()
                .map(category -> this.magicMapper.map(category, CategoryViewModel.class))
                .collect(Collectors.toList());
        DropDownCategoryViewModel viewModel = new DropDownCategoryViewModel();
        viewModel.setCategories(categories);
        return view(viewModel, model);
    }

    @GetMapping(value = URIMappings.URI_CATEGORY_NAMES)
    public
    @ResponseBody
    String allCategories() {
        return new Gson().toJson(this.categoryService.getAllCategoriesNames());
    }

    @GetMapping(value = "/categories/all")
    public String categoriesAll(Model model) {


        List<CategoryViewModel> categories = this.categoryService.findAll()
                .stream()
                .map(category -> this.magicMapper.map(category, CategoryViewModel.class))
                .collect(Collectors.toList());
        DropDownCategoryViewModel viewModel = new DropDownCategoryViewModel();
        viewModel.setCategories(categories);
        return view(viewModel, model);
    }
}
