package app.controllers.admin;

import app.constants.Permissions;
import app.constants.URIMappings;
import app.controllers.AbstractController;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.article.CreateArticleBindingModel;
import app.models.dto.bindingModel.article.EditArticleBindingModel;
import app.models.dto.viewModel.article.EditArticleViewModel;
import app.models.dto.viewModel.article.GetArticleViewModel;
import app.models.dto.viewModel.facebook.TokenViewModel;
import app.models.entity.Article;
import app.models.entity.User;
import app.services.article.ArticleService;
import app.services.pagers.PageableService;
import app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookLink;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static app.constants.Patterns.REGEX_MATCH_HTML_ATTRIBUTES;

@Controller
@PreAuthorize(Permissions.PERMISSION_IS_ADMIN)
public class ArticleAdminController extends AbstractController {

    private static final int ARTICLES_PER_PAGE = 12;

    private final PageableService pageableService;
    private final ArticleService articleService;
    private final MagicMapper magicMapper;
    private final UserService userService;


    @Autowired
    public ArticleAdminController(PageableService pageableService, ArticleService articleService,
                                  MagicMapper magicMapper,
                                  UserService userService) {
        this.pageableService = pageableService;
        this.articleService = articleService;
        this.magicMapper = magicMapper;
        this.userService = userService;
    }

    @GetMapping(URIMappings.URI_ALL_ARTICLES)
    public String allArticles(Model model) {
        User currentAdmin = this.userService.getCurrentAdmin();

        if (currentAdmin != null) {

            TokenViewModel viewModel = new TokenViewModel(currentAdmin.getHasValidToken());
            return view(viewModel, model);
        }

        return view(new TokenViewModel(false), model);
    }

    @GetMapping(value = URIMappings.URI_ARTICLE_CREATE)
    public String articleCreate() {
        return view();
    }

    @PostMapping(value = URIMappings.URI_ARTICLE_CREATE)
    public String articleCreate(CreateArticleBindingModel model) throws IOException, NoSuchFieldException, IllegalAccessException {
        String message = model.getPostContent();
        String facebookId = "";
        User admin = this.userService.getCurrentAdmin();
        if (message != null && !"".equals(message.trim()) && admin != null) {
            String postLink = String.format(URIMappings.URI_BLOG_ARTICLE_LINK,
                    this.articleService.getLastId() + 1L);
            try {
                Facebook facebook = new FacebookTemplate(this.userService.getAccessToken());
                facebookId = facebook.feedOperations()
                        .postLink(message,
                                new FacebookLink(postLink,
                                        model.getName(), "FOODIEHOUR.COM",
                                        model.getBody().replaceAll(REGEX_MATCH_HTML_ATTRIBUTES, "")));
                admin.setHasValidToken(true);
                this.userService.createAdmin(admin);
            } catch (Exception e) {
                admin.setHasValidToken(false);
                this.userService.createAdmin(admin);
            }
        }
        model.setFacebookId(facebookId);
        this.articleService.create(model);
        return redirect(URIMappings.URI_ALL_ARTICLES);
    }

    @GetMapping(URIMappings.URI_ARTICLES_ALL_PAGEABLE_ADMIN)
    public String articleTable(@PageableDefault(ARTICLES_PER_PAGE) Pageable pageable,
                               Model model,
                               HttpServletRequest request) throws InstantiationException, IllegalAccessException {
        GetArticleViewModel viewModel =
                this.pageableService.getPageableViewModel(
                        pageable,
                        this.articleService.findAll(pageable).getContent(),
                        GetArticleViewModel.class,
                        this.articleService.getCount(),
                        Integer.parseInt(request.getParameter("page"))
                );

        return view(viewModel, model);
    }

    @GetMapping(URIMappings.URI_ARTICLE_DELETE)
    public String deleteArticle(@PathVariable("id") Long articleId) {
        Article article = this.articleService.findOne(articleId);
        if (article.getFacebookId() != null && !article.getFacebookId().equals("")) {
            Facebook facebook = new FacebookTemplate(this.userService.getAccessToken());
            facebook.delete(article.getFacebookId());
        }
        this.articleService.delete(articleId);
        return redirect(URIMappings.URI_ALL_ARTICLES);
    }

    @GetMapping(URIMappings.URI_ARTICLE_EDIT)
    public String articleEdit(@PathVariable("id") Long articleId,
                              Model model) {
        Article article = this.articleService.findOne(articleId);
        EditArticleViewModel viewModel = this.magicMapper.map(
                this.articleService.findOne(articleId),
                EditArticleViewModel.class
        );

        return view(viewModel, model);
    }

    @PostMapping(URIMappings.URI_ARTICLE_EDIT)
    public String articleEditProcess(EditArticleBindingModel model,
                                     @PathVariable("id") Long articleId) {
        model.setId(articleId);
        this.articleService.edit(model);
        return redirect(URIMappings.URI_ALL_ARTICLES);
    }
}
