package app.controllers.admin;

import app.constants.Permissions;
import app.constants.URIMappings;
import app.controllers.AbstractController;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.recipe.CreateRecipeBindingModel;
import app.models.dto.bindingModel.recipe.RecipeEditBindingModel;
import app.models.dto.viewModel.facebook.TokenViewModel;
import app.models.dto.viewModel.recipe.RecipeEditViewModel;
import app.models.dto.viewModel.recipe.RecipeListViewModel;
import app.models.entity.Recipe;
import app.models.entity.User;
import app.services.pagers.PageableService;
import app.services.recipe.RecipeService;
import app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookLink;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static app.constants.Patterns.REGEX_MATCH_HTML_ATTRIBUTES;

@Controller
@PreAuthorize(Permissions.PERMISSION_IS_ADMIN)
public class RecipeAdminController extends AbstractController {

    private static final int RECIPE_PER_PAGE = 12;
    private static final int PAGE_ON_SLIDER = 3;

    private final PageableService pageableService;
    private final RecipeService recipeService;
    private final MagicMapper magicMapper;
    private final UserService userService;


    @Autowired
    public RecipeAdminController(PageableService pageableService,
                                 RecipeService recipeService,
                                 MagicMapper magicMapper,
                                 UserService userService) {
        this.pageableService = pageableService;
        this.recipeService = recipeService;
        this.magicMapper = magicMapper;
        this.userService = userService;
    }

    @GetMapping(URIMappings.URI_RECIPES_ALL_ADMIN)
    public String allRecipes(Model model) {

        User currentAdmin = this.userService.getCurrentAdmin();

        if (currentAdmin != null) {

            TokenViewModel viewModel = new TokenViewModel(currentAdmin.getHasValidToken());
            return view(viewModel, model);
        }

        return view(new TokenViewModel(false), model);
    }

    @GetMapping(value = URIMappings.URI_RECIPE_CREATE_ADMIN)
    public String recipeCreate() {
        return view();
    }

    @PostMapping(value = URIMappings.URI_RECIPE_CREATE_ADMIN)
    public String recipeCreate(CreateRecipeBindingModel model) throws IOException, NoSuchFieldException, IllegalAccessException {
        String message = model.getPostContent();
        String facebookId = "";
        User admin = this.userService.getCurrentAdmin();
        if (message != null && !"".equals(message.trim()) && admin != null) {
            String postLink = String.format(URIMappings.URI_RECIPE_ARTICLE_LINK,
                    this.recipeService.getLastId() + 1L);
            try {
                Facebook facebook = new FacebookTemplate(this.userService.getAccessToken());
                facebookId = facebook.feedOperations()
                        .postLink(message,
                                new FacebookLink(postLink, model.getName(),
                                        "FOODIEHOUR.COM", model.getBody().replaceAll(REGEX_MATCH_HTML_ATTRIBUTES, "")));
                admin.setHasValidToken(true);
                this.userService.createAdmin(admin);
            } catch (Exception e) {

                admin.setHasValidToken(false);
                this.userService.createAdmin(admin);
                e.printStackTrace();
            }
        }
        model.setFacebookId(facebookId);
        this.recipeService.create(model);

        return redirect(URIMappings.URI_RECIPES_ALL_ADMIN);
    }

    @GetMapping(URIMappings.URI_RECIPES_ALL_PAGEABLE_ADMIN)
    public String recipesTable(@PageableDefault(RECIPE_PER_PAGE) Pageable pageable, Model model, HttpServletRequest request) throws InstantiationException, IllegalAccessException {
        RecipeListViewModel viewModel =
                this.pageableService.getPageableViewModel(
                        pageable,
                        this.recipeService.findAll(pageable).getContent(),
                        RecipeListViewModel.class,
                        this.recipeService.getCount(),
                        Integer.parseInt(request.getParameter("page"))
                );

        return view(viewModel, model);
    }

    @GetMapping(URIMappings.URI_RECIPE_DELETE_ADMIN)
    public String deleteRecipe(@PathVariable("id") Long recipeId) {
        Recipe recipe = this.recipeService.findOneById(recipeId);
        if (recipe.getFacebookId() != null && !recipe.getFacebookId().equals("")) {
            Facebook facebook = new FacebookTemplate(this.userService.getAccessToken());
            facebook.delete(recipe.getFacebookId());
        }
        this.recipeService.delete(recipeId);
        return redirect(URIMappings.URI_RECIPES_ALL_ADMIN);
    }

    @GetMapping(URIMappings.URI_RECIPE_EDIT_ADMIN)
    public String recipeEdit(@PathVariable("id") Long recipeId,
                             Model model) {
        RecipeEditViewModel viewModel = this.magicMapper.map(
                this.recipeService.findOneById(recipeId),
                RecipeEditViewModel.class
        );

        return view(viewModel, model);
    }

    @PostMapping(URIMappings.URI_RECIPE_EDIT_ADMIN)
    public String recipeEditProcess(RecipeEditBindingModel model, @PathVariable("id") Long recipeId) throws IOException {
        model.setId(recipeId);
        this.recipeService.edit(recipeId, model);

        return redirect(URIMappings.URI_RECIPES_ALL_ADMIN);
    }
}
