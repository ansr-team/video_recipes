package app.controllers.admin;


import app.constants.Permissions;
import app.constants.URIMappings;
import app.controllers.AbstractController;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.category.CreateCategoryBindingModel;
import app.models.dto.bindingModel.category.EditCategoryBindingModel;
import app.models.dto.viewModel.category.EditCategoryViewModel;
import app.models.dto.viewModel.category.ListCategoryViewModel;
import app.models.dto.viewModel.facebook.TokenViewModel;
import app.models.entity.User;
import app.services.category.CategoryService;
import app.services.pagers.PageableService;
import app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@PreAuthorize(Permissions.PERMISSION_IS_ADMIN)
public class CategoryAdminController extends AbstractController {


    private static final int CATEGORY_PER_PAGE = 12;
    private static final int PAGE_ON_SLIDER = 3;

    private final CategoryService categoryService;
    private final PageableService pageableService;
    private final MagicMapper magicMapper;
    private final UserService userService;


    @Autowired
    public CategoryAdminController(CategoryService categoryService,
                                   PageableService pageableService,
                                   MagicMapper magicMapper,
                                   UserService userService) {
        this.categoryService = categoryService;
        this.pageableService = pageableService;
        this.magicMapper = magicMapper;
        this.userService = userService;
    }

    @GetMapping(URIMappings.URI_CATEGORY_CREATE)
    public String categoryCreate() {
        return view();
    }

    @PostMapping(value = URIMappings.URI_CATEGORY_CREATE)
    public String categoryCreateProcess(CreateCategoryBindingModel model) throws IOException {

        this.categoryService.create(model);

        return redirect(URIMappings.URI_ALL_CATEGORIES);
    }

    @GetMapping(URIMappings.URI_ALL_CATEGORIES)
    public String allCategories(Model model) {
        User currentAdmin = this.userService.getCurrentAdmin();
        TokenViewModel viewModel = null;
        if (currentAdmin != null) {

            viewModel = new TokenViewModel(currentAdmin.getHasValidToken());
            model.addAttribute("model", viewModel);
        } else {
            viewModel = new TokenViewModel(false);
            model.addAttribute("model", viewModel);
        }

        return "category_admin/categories_all";
    }

    @GetMapping(URIMappings.URI_ALL_CATEGORIES_PAGEABLE)
    public String categoriesTable(@PageableDefault(CATEGORY_PER_PAGE) Pageable pageable, Model model, HttpServletRequest request) throws InstantiationException, IllegalAccessException {
        ListCategoryViewModel viewModel =
                this.pageableService.getPageableViewModel(
                        pageable,
                        this.categoryService.findAll(pageable).getContent(),
                        ListCategoryViewModel.class,
                        this.categoryService.getCount(),
                        Integer.parseInt(request.getParameter("page"))
                );

        return view(viewModel, model);
    }

    @GetMapping(URIMappings.URI_CATEGORY_DELETE)
    public String deleteCategory(@PathVariable("id") Long categoryId) {
        this.categoryService.deleteCategory(categoryId);
        return redirect(URIMappings.URI_ALL_CATEGORIES);
    }

    @GetMapping(URIMappings.URI_CATEGORY_EDIT)
    public String categoryEdit(@PathVariable("id") Long categoryId,
                               Model model) {
        EditCategoryViewModel viewModel = this.magicMapper.map(
                this.categoryService.findOneById(categoryId),
                EditCategoryViewModel.class
        );

        return view(viewModel, model);
    }

    @PostMapping(URIMappings.URI_CATEGORY_EDIT)
    public String categoryEdit(@PathVariable("id") Long categoryId,
                               EditCategoryBindingModel model) throws IOException {
        this.categoryService.edit(categoryId, model);

        return redirect(URIMappings.URI_ALL_CATEGORIES);
    }
}
