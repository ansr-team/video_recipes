package app.controllers.admin;

import app.constants.Permissions;
import app.constants.URIMappings;
import app.controllers.AbstractController;
import app.models.dto.viewModel.facebook.TokenViewModel;
import app.models.entity.User;
import app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static app.constants.AdminData.ADMIN_USERNAME;
import static app.constants.URIMappings.UPDATE_TOKEN;

@Controller
public class AdminController extends AbstractController {

    private final UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(URIMappings.URI_ADMIN_LOGIN)
    public String login() {
        return view();
    }

    @PreAuthorize(Permissions.PERMISSION_IS_ADMIN)
    @GetMapping(URIMappings.URI_ADMIN_MAIN_VIEW)
    public String mainView(Model model) {

        User currentAdmin = this.userService.getCurrentAdmin();

        if (currentAdmin != null) {

            TokenViewModel viewModel = new TokenViewModel(currentAdmin.getHasValidToken());
            return view(viewModel, model);
        }

        return view(new TokenViewModel(false), model);
    }

    @GetMapping(URIMappings.URI_ADMIN_LOGOUT)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return redirect(URIMappings.URI_HOME_INDEX);
    }

    @PreAuthorize(Permissions.PERMISSION_IS_ADMIN)
    @PostMapping(UPDATE_TOKEN)
    public String updateToken(@RequestParam("token") String token) {
        User admin = this.userService.findOneByUsername(ADMIN_USERNAME);
        admin.setAccessToken(token);
        admin.setHasValidToken(true);
        this.userService.createAdmin(admin);
        return redirect(URIMappings.URI_ADMIN_MAIN_VIEW);
    }
}
