package app.controllers;

import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.viewModel.article.ArticlePreviewViewModel;
import app.models.dto.viewModel.home.HomeViewModel;
import app.models.dto.viewModel.video.VideoViewModel;
import app.models.entity.Article;
import app.models.entity.Recipe;
import app.services.article.ArticleService;
import app.services.recipe.RecipeService;
import app.services.user.UserService;
import app.services.video.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.stream.Collectors;

import static app.constants.SecurityConstants.APPLICATION_ID;
import static app.constants.SecurityConstants.TWO_HOUR;

@Controller
public class HomeController extends AbstractController {

    private final VideoService videoService;
    private final ArticleService articleService;
    private final RecipeService recipeService;
    private final MagicMapper magicMapper;
    private final UserService userService;

    @Autowired
    public HomeController(VideoService videoService,
                          ArticleService articleService,
                          RecipeService recipeService,
                          MagicMapper magicMapper,
                          UserService userService) {
        this.videoService = videoService;
        this.articleService = articleService;
        this.recipeService = recipeService;
        this.magicMapper = magicMapper;
        this.userService = userService;
    }

    @GetMapping(URIMappings.URI_HOME_INDEX)
    public String index(Model model) {
        List<VideoViewModel> top3Videos = this.videoService.getTopThreeVideos()
                .stream()
                .map(video -> this.magicMapper.map(video, VideoViewModel.class))
                .collect(Collectors.toList());

        List<ArticlePreviewViewModel> articlePreviewViewModels = this.articleService.getTopTwelveByRank()
                .stream()
                .map(article -> this.magicMapper.map(article, ArticlePreviewViewModel.class))
                .collect(Collectors.toList());

        HomeViewModel viewModel = new HomeViewModel();
        viewModel.setTopEightVideos(top3Videos);
        viewModel.setLeftColumnArticles(articlePreviewViewModels.subList(0, articlePreviewViewModels.size() / 2));
        viewModel.setRightColumnArticles(articlePreviewViewModels
                .subList(articlePreviewViewModels.size() / 2, articlePreviewViewModels.size()));
        return view(viewModel, model);
    }

    @Scheduled(fixedDelay = TWO_HOUR)
    private void updateFacebookItems() {
        try {
            Facebook facebook = new FacebookTemplate(this.userService.getAccessToken(),
                    null,
                    APPLICATION_ID);

            this.updateRecipeRating(facebook);
            this.updateArticleRating(facebook);
        } catch (Exception e) {
           this.userService.getAdminProfiles().forEach(admin -> {
               admin.setHasValidToken(false);
               this.userService.createAdmin(admin);
           });
        }
    }

    private void updateRecipeRating(Facebook facebook) {
        List<Recipe> recipes = this.recipeService.findAllFromFacebook();

        for (Recipe recipe : recipes) {
            if (!recipe.getFacebookId().equals("")) {
                PagedList<Reference> likes = facebook.likeOperations()
                        .getLikes(recipe.getFacebookId());
                long rating = likes.size();
                recipe.setRating(rating);
                recipe.getVideo().setRating(rating);
                this.recipeService.updateFacebookRating(recipe);
            }
        }
    }

    private void updateArticleRating(Facebook facebook) {
        List<Article> articles = this.articleService.findAllFromFacebook();
        for (Article article : articles) {
            if (!article.getFacebookId().equals("")) {

                PagedList<Reference> likes = facebook.likeOperations()
                        .getLikes(article.getFacebookId());
                long rating = likes.size();
                article.setRating(rating);
                this.articleService.updateFacebookRating(article);
            }
        }
    }
}
