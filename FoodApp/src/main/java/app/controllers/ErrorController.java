package app.controllers;

import app.constants.URIMappings;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController extends AbstractController{

    @GetMapping(value = URIMappings.ACCESS_DENIED)
    public String accessDenied() {
        return view();
    }
}
