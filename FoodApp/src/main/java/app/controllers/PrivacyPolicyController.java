package app.controllers;

import app.constants.URIMappings;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PrivacyPolicyController extends AbstractController {

    @GetMapping(URIMappings.URI_PRIVACY_POLICY)
    public String privacyPolicy() {

        return view();
    }
}
