package app.controllers;

import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.search.SearchBindingModel;
import app.models.dto.bindingModel.search.SearchOrderedBindingModel;
import app.models.dto.viewModel.recipe.RecipeSearchViewModel;
import app.models.dto.viewModel.search.SearchViewModel;
import app.models.dto.viewModel.video.VideoSearchViewModel;
import app.services.recipe.RecipeService;
import app.services.video.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class SearchController extends AbstractController {

    private final VideoService videoService;
    private final MagicMapper magicMapper;
    private final RecipeService recipeService;

    @Autowired
    public SearchController(VideoService videoService,
                            MagicMapper magicMapper,
                            RecipeService recipeService) {
        this.videoService = videoService;
        this.magicMapper = magicMapper;
        this.recipeService = recipeService;
    }

    @GetMapping(URIMappings.URI_SEARCH)
    public String search(SearchBindingModel bindingModel, Model model, HttpServletRequest request) {

        Map<String, String[]> parameterMap = request.getParameterMap();

        if (parameterMap.size() > 1) {
            SearchOrderedBindingModel searchOrderedBindingModel = new SearchOrderedBindingModel();

            String[] searchKeyWords = parameterMap.get("searchKeyWords");
            searchOrderedBindingModel.setSearchKeyWords(searchKeyWords[0]);

            int videoPage = Integer.parseInt(parameterMap.get("videoPage")[0]);
            int recipePage = Integer.parseInt(parameterMap.get("recipePage")[0]);

            if (parameterMap.containsKey("order")){
                String[] order = parameterMap.get("order");
                searchOrderedBindingModel.setOrder(order[0]);
            }

            if (parameterMap.containsKey("categoryNames")) {
                String[] categories = parameterMap.get("categoryNames");
                searchOrderedBindingModel.setCategoryNames(Arrays.asList(categories[0].split(",")));
            }

            searchOrderedBindingModel.setVideoPage(videoPage <= 1 ? 0:videoPage - 1);
            searchOrderedBindingModel.setRecipePage(recipePage <= 1 ? 0:recipePage - 1);
            return this.searchPathLink(searchOrderedBindingModel,model,request);
        }

        String[] keyWords = bindingModel.getSearchKeyWords().split("[^\\w+|\\d+]");

        SearchViewModel searchViewModel = new SearchViewModel();

        List<VideoSearchViewModel> videoResults = new ArrayList<>();
        List<RecipeSearchViewModel> recipesResults = new ArrayList<>();

        if (keyWords.length > 0) {
            videoResults = this.videoService
                    .search(keyWords)
                    .stream()
                    .map(e -> this.magicMapper.map(e, VideoSearchViewModel.class))
                    .collect(Collectors.toList());
            recipesResults = this.recipeService.findAllByKeyWords(keyWords)
                    .stream()
                    .map(e -> this.magicMapper.map(e, RecipeSearchViewModel.class))
                    .collect(Collectors.toList());
            searchViewModel.setKeyWords(keyWords);
            searchViewModel.setVideoPage(bindingModel.getVideoPage());
            searchViewModel.setRecipePage(bindingModel.getRecipePage());
        }
        searchViewModel.setVideos(videoResults);
        searchViewModel.setRecipes(recipesResults);

        return view(searchViewModel, model);
    }

    @GetMapping(URIMappings.URI_SEARCH_ORDERED)
    public String orderedResult(SearchOrderedBindingModel bindingModel, Model model, HttpServletRequest request) {

        SearchViewModel searchViewModel = getSearchViewModel(bindingModel, request);
        model.addAttribute("model", searchViewModel);
        return view(searchViewModel, model);

    }

    private String searchPathLink(SearchOrderedBindingModel bindingModel, Model model, HttpServletRequest request) {
        SearchViewModel searchViewModel = getSearchViewModel(bindingModel, request);
        model.addAttribute("model", searchViewModel);
        return "search/search";
    }

    private SearchViewModel getSearchViewModel(SearchOrderedBindingModel bindingModel, HttpServletRequest request) {
        String[] keyWords = bindingModel.getSearchKeyWords().split("[^\\w+|\\d+]");

        String orderBy = bindingModel.getOrder();
        String order;
        switch (orderBy.toLowerCase()) {
            case "date": order = "createdOn DESC";
                break;
            case "category": order = "cat";
                break;
            case "rating": order = "rating DESC";
                break;
            default: order = "name";
        }

        SearchViewModel searchViewModel = new SearchViewModel();

        searchViewModel.setQueryPath(request.getQueryString());

        List<VideoSearchViewModel> videoResults = new ArrayList<>();
        List<RecipeSearchViewModel> recipesResults = new ArrayList<>();

        if (keyWords.length > 0 && !order.equals("cat")) {
            videoResults = this.videoService
                    .search(order, keyWords)
                    .stream()
                    .map(e -> this.magicMapper.map(e, VideoSearchViewModel.class))
                    .collect(Collectors.toList());
            recipesResults = this.recipeService.findAllByKeyWords(order, keyWords)
                    .stream()
                    .map(e -> this.magicMapper.map(e, RecipeSearchViewModel.class))
                    .collect(Collectors.toList());
            searchViewModel.setKeyWords(keyWords);
            searchViewModel.setOrder(orderBy);
            searchViewModel.setRecipePage(bindingModel.getRecipePage());
            searchViewModel.setVideoPage(bindingModel.getVideoPage());
        } else {
            videoResults = this.videoService
                    .search(order, bindingModel.getCategoryNames(),keyWords)
                    .stream()
                    .map(e -> this.magicMapper.map(e, VideoSearchViewModel.class))
                    .collect(Collectors.toList());
            recipesResults = this.recipeService.findAllByKeyWords(order,bindingModel.getCategoryNames(), keyWords)
                    .stream()
                    .map(e -> this.magicMapper.map(e, RecipeSearchViewModel.class))
                    .collect(Collectors.toList());
            searchViewModel.setKeyWords(keyWords);
            searchViewModel.setOrder(orderBy);
            searchViewModel.setRecipePage(bindingModel.getRecipePage());
            searchViewModel.setVideoPage(bindingModel.getVideoPage());
        }
        searchViewModel.setVideos(videoResults);
        searchViewModel.setRecipes(recipesResults);
        return searchViewModel;
    }

}
