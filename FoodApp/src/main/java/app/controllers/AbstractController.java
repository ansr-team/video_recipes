package app.controllers;

import org.springframework.ui.Model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractController {

    private static final String KEY_VIEWMODEL = "model";
    private static final String KEY_BINDINGMODEL = "bindingModel";
    private static final String CONTROLLER_SUFFIX = "Controller";
    private static final String ACTION_SUFFIX = "Action";
    private static final String VIEW_SEPARATOR = "/";
    private static final String STACKTRACE_EXCLUDED_METHOD = "view";
    private static final String REDIRECT_PREFIX = "redirect:";

    protected String view(String name, Object viewModel, Model model, Object bindingModel) {
        if (model != null) {
            model.addAttribute(
                    AbstractController.KEY_VIEWMODEL,
                    viewModel);
            if (bindingModel != null) {
                model.addAttribute(
                        AbstractController.KEY_BINDINGMODEL,
                        bindingModel
                );
            }
        }

        return name;
    }

    protected String view(String name, Object viewModel, Model model) {
        return this.view(name, viewModel, model, null);
    }

    protected String view(Model model, Object bindingModel) {
        return this.view(null, model, bindingModel);
    }

    protected String view(Object viewModel, Model model) {
        return this.view(viewModel, model, null);
    }

    protected String view(Object viewModel, Model model, Object bindingModel) {
        String packageName = this.getClass().getName().replace(this.getClass().getSimpleName(), "");
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        int i = 0;
        StackTraceElement lastFrame = stackTrace[i];

        while (!lastFrame.getClassName().endsWith(AbstractController.CONTROLLER_SUFFIX)
                || lastFrame.getMethodName().equals(AbstractController.STACKTRACE_EXCLUDED_METHOD)) {
            lastFrame = stackTrace[++i];
        }

        String controllerName = this.toSnakeCase(
                lastFrame
                        .getClassName()
                        .replace(
                                packageName,
                                ""
                        )
                        .replace(
                                AbstractController.CONTROLLER_SUFFIX,
                                ""
                        )
        );

        String methodName = this.toSnakeCase(
                lastFrame
                        .getMethodName()
                        .replace(
                                AbstractController.ACTION_SUFFIX,
                                ""
                        )
        );

        String viewName = String.format("%s%s%s",
                controllerName,
                AbstractController.VIEW_SEPARATOR,
                methodName
        );

        return this.view(viewName, viewModel, model, bindingModel);
    }

    protected String view() {
        return this.view((Object) null, null, null);
    }

    protected String view(String name) {
        return this.view(name, (Object) null, null);
    }

    protected String redirect(String URI, Object... arg) {
        Pattern pattern = Pattern.compile("\\{[^{}]+\\}");
        Matcher matcher = pattern.matcher(URI);
        int index = 0;
        while (matcher.find()) {
            URI = URI.replace(matcher.group(), arg[index].toString());
            index++;
        }
        return REDIRECT_PREFIX + URI;
    }

    protected String toSnakeCase(String camelCase) {
        String result = (camelCase.charAt(0) + "").toLowerCase();
        for (int i = 1; i < camelCase.length(); i++) {
            if (Character.isUpperCase(camelCase.charAt(i))) {
                result += "_" + (camelCase.charAt(i) + "").toLowerCase();
            } else {
                result += camelCase.charAt(i);
            }
        }

        return result;
    }


}
