package app.controllers;

import app.constants.ComponentFolders;
import app.constants.URIMappings;
import app.helper.magicMapper.MagicMapper;
import app.models.dto.bindingModel.file.UploadFileBindingModel;
import app.models.dto.viewModel.recipe.RecipePerCategoryViewModel;
import app.models.dto.viewModel.recipe.RecipeShowSingleViewModel;
import app.models.dto.viewModel.recipe.RecipeVideoViewModel;
import app.models.entity.Recipe;
import app.services.category.CategoryService;
import app.services.file.FileService;
import app.services.pagers.PageableService;
import app.services.picture.PictureService;
import app.services.recipe.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class RecipeController extends AbstractController {

    private final RecipeService recipeService;
    private final CategoryService categoryService;
    private final MagicMapper magicMapper;
    private final FileService fileService;
    private final PictureService pictureService;
    private final PageableService pageableService;

    private static final int RECIPES_ON_PAGE = 8;

    @Autowired
    public RecipeController(RecipeService recipeService, CategoryService categoryService, MagicMapper magicMapper, FileService fileService, PictureService pictureService, PageableService pageableService) {
        this.recipeService = recipeService;
        this.categoryService = categoryService;
        this.magicMapper = magicMapper;
        this.fileService = fileService;
        this.pictureService = pictureService;
        this.pageableService = pageableService;
    }

    @PostMapping(URIMappings.URI_RECIPES_PICTURE_UPLOAD)
    public
    @ResponseBody
    String uploadPicture(UploadFileBindingModel model) throws IOException, InterruptedException {
        String path = this.fileService.upload(model.getUpload(), ComponentFolders.DIRECTORY_RECIPE);
        this.pictureService.save(path);

        return "<script>window.parent.CKEDITOR.tools.callFunction('1','" + path + "');</script>";
    }

    @GetMapping(URIMappings.URI_RECIPE_SHOW)
    public String showRecipe(@PathVariable("id") Long recipeId, Model model) {

        Recipe recipe = this.recipeService.findOneById(recipeId);
        RecipeShowSingleViewModel viewRecipe = this.magicMapper.map(recipe, RecipeShowSingleViewModel.class);

        return view(viewRecipe, model);
    }

    @GetMapping(value = URIMappings.URI_RECIPES_PER_CATEGORY)
    public String recipesByPage(@PageableDefault(RECIPES_ON_PAGE) Pageable pageable,
                                Model model,
                                HttpServletRequest request,
                                @PathVariable("id") long categoryId) throws InstantiationException, IllegalAccessException {
//        RecipePerCategoryViewModel viewModel =
//                this.pageableService.getPageableViewModel(
//                        pageable,
//                        this.recipeService.findAllByCategoryId(categoryId, pageable)
//                                .getContent(),
//                        RecipePerCategoryViewModel.class,
//                        this.categoryService.recipesCount(categoryId),
//                        Integer.parseInt(request.getParameter("page"))
//                );

//        return view(viewModel, model);
        Integer page = Integer.parseInt(request.getParameter("page"));
        RecipeVideoViewModel viewModel = this.pageableService
                .getPageableViewModel(
                        pageable,
                        this.recipeService.findAllByCategoryId(categoryId,pageable).getContent(),
                        RecipeVideoViewModel.class,
                        this.categoryService.recipesCount(categoryId),
                        page);

        return view(viewModel, model);
    }
}
