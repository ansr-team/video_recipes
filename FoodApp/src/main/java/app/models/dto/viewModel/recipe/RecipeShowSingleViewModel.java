package app.models.dto.viewModel.recipe;

public class RecipeShowSingleViewModel {

    private Long recipeId;
    private String name;
    private String body;
    private String picturePath;
    private String videoLinkId;
    private Long rating;
    private String facebookId;

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getVideoLinkId() {
        return this.videoLinkId;
    }

    public void setVideoLinkId(String videoLinkId) {
        this.videoLinkId = videoLinkId;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }
}
