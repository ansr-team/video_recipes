package app.models.dto.viewModel.facebook;

public class TokenViewModel {

    boolean isValid;

    public TokenViewModel(boolean isValid) {
        this.isValid = isValid;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
