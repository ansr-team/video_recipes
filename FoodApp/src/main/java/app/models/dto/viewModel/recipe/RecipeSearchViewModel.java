package app.models.dto.viewModel.recipe;

import app.models.dto.viewModel.PageableViewModel;
import app.models.dto.viewModel.category.ListCategoryViewModel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class RecipeSearchViewModel extends PageableViewModel<RecipeSearchViewModel> {

    private Long id;
    private String name;
    private String body;
    private Date createdOn;
    private Set<ListCategoryViewModel> categories = new HashSet<>(0);
    private String picturePath;
    private Long raiting;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Set<ListCategoryViewModel> getCategories() {
        return categories;
    }

    public void setCategories(Set<ListCategoryViewModel> categories) {
        this.categories = categories;
    }

    public Long getRaiting() {
        return raiting;
    }

    public void setRaiting(Long raiting) {
        this.raiting = raiting;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
