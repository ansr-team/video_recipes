package app.models.dto.viewModel.recipe;

import app.models.dto.viewModel.PageableViewModel;

public class RecipeVideoViewModel extends PageableViewModel<RecipeVideoViewModel> {

    private Long videoId;
    private String videoPath;
    private String videoName;
    private String videoLinkId;
    private Long recipeId;
    private String recipeName;

    public Long getVideoId() {
        return videoId;
    }

    public void setVideoId(Long videoId) {
        this.videoId = videoId;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoLinkId() {
        return videoLinkId;
    }

    public void setVideoLinkId(String videoLinkId) {
        this.videoLinkId = videoLinkId;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }
}
