package app.models.dto.viewModel.blog;

import app.models.dto.viewModel.article.ArticlePreviewViewModel;

public class BlogViewModel {
    ArticlePreviewViewModel mostRecent;

    public ArticlePreviewViewModel getMostRecent() {
        return mostRecent;
    }

    public void setMostRecent(ArticlePreviewViewModel mostRecent) {
        this.mostRecent = mostRecent;
    }
}
