package app.models.dto.viewModel.category;

import app.models.dto.viewModel.PageableViewModel;

public class ListCategoryViewModel extends PageableViewModel<ListCategoryViewModel>{

    private Long id;
    private String name;

    public ListCategoryViewModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
