package app.models.dto.viewModel.home;

import app.models.dto.viewModel.article.ArticlePreviewViewModel;
import app.models.dto.viewModel.video.VideoViewModel;

import java.util.List;

public class HomeViewModel {

    private List<VideoViewModel> topEightVideos;
    private List<ArticlePreviewViewModel> leftColumnArticles;
    private List<ArticlePreviewViewModel> rightColumnArticles;

    public List<VideoViewModel> getTopEightVideos() {
        return topEightVideos;
    }

    public void setTopEightVideos(List<VideoViewModel> topEightVideos) {
        this.topEightVideos = topEightVideos;
    }

    public List<ArticlePreviewViewModel> getLeftColumnArticles() {
        return leftColumnArticles;
    }

    public void setLeftColumnArticles(List<ArticlePreviewViewModel> leftColumnArticles) {
        this.leftColumnArticles = leftColumnArticles;
    }

    public List<ArticlePreviewViewModel> getRightColumnArticles() {
        return rightColumnArticles;
    }

    public void setRightColumnArticles(List<ArticlePreviewViewModel> rightColumnArticles) {
        this.rightColumnArticles = rightColumnArticles;
    }
}
