package app.models.dto.viewModel.category;

import java.util.List;

public class DropDownCategoryViewModel {
    List<CategoryViewModel> categories;

    public List<CategoryViewModel> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryViewModel> categories) {
        this.categories = categories;
    }
}
