package app.models.dto.viewModel.category;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CategoryViewModel {

    private Long id;
    private String picturePath;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id)
    {


        this.id = id;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
