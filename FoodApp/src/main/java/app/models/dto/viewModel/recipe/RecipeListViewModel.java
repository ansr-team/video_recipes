package app.models.dto.viewModel.recipe;

import app.models.dto.viewModel.PageableViewModel;

public class RecipeListViewModel extends PageableViewModel<RecipeListViewModel>{

    private Long id;
    private String name;

    public RecipeListViewModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
