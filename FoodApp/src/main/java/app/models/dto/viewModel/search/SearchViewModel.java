package app.models.dto.viewModel.search;

import app.models.dto.viewModel.PageableViewModel;
import app.models.dto.viewModel.recipe.RecipeSearchViewModel;
import app.models.dto.viewModel.video.VideoSearchViewModel;

import java.util.ArrayList;
import java.util.List;

public class SearchViewModel extends PageableViewModel<SearchViewModel> {

    private String[] keyWords;

    private String order = "";

    private List<VideoSearchViewModel> videos = new ArrayList<>();

    private List<RecipeSearchViewModel> recipes = new ArrayList<>();

    private String queryPath = "";

    private int videoPage;

    private int recipePage;

    public List<VideoSearchViewModel> getVideos() {
        return videos;
    }

    public void setVideos(List<VideoSearchViewModel> videos) {
        this.videos = videos;
    }

    public List<RecipeSearchViewModel> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeSearchViewModel> recipes) {
        this.recipes = recipes;
    }

    public boolean hasVideoResults() {
        return !this.videos.isEmpty();
    }

    public boolean hasRecipeResults() {
        return !this.recipes.isEmpty();
    }

    public String[] getKeyWords() {
        return this.keyWords;
    }

    public void setKeyWords(String[] keyWords) {
        this.keyWords = keyWords;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getQueryPath() {
        return queryPath;
    }

    public void setQueryPath(String queryPath) {
        this.queryPath = queryPath;
    }

    public int getVideoPage() {
        return videoPage;
    }

    public void setVideoPage(int videoPage) {
        this.videoPage = videoPage;
    }

    public int getRecipePage() {
        return recipePage;
    }

    public void setRecipePage(int recipePage) {
        this.recipePage = recipePage;
    }
}
