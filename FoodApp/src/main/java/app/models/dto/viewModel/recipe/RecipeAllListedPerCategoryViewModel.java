package app.models.dto.viewModel.recipe;

import java.util.List;

public class RecipeAllListedPerCategoryViewModel {

    private List<RecipePerCategoryViewModel> newestRecipes;
    private List<RecipePerCategoryViewModel> allRecipes;

    public List<RecipePerCategoryViewModel> getAllRecipes() {
        return allRecipes;
    }

    public void setAllRecipes(List<RecipePerCategoryViewModel> allRecipes) {
        this.allRecipes = allRecipes;
    }

    public List<RecipePerCategoryViewModel> getNewestRecipes() {
        return newestRecipes;
    }

    public void setNewestRecipes(List<RecipePerCategoryViewModel> newestRecipes) {
        this.newestRecipes = newestRecipes;
    }
}
