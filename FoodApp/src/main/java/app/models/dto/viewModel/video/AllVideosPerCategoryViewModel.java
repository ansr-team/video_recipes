package app.models.dto.viewModel.video;

import java.util.HashSet;
import java.util.Set;

public class AllVideosPerCategoryViewModel {

    Set<VideoViewModel> videos=new HashSet<>();

    public Set<VideoViewModel> getVideos() {
        return videos;
    }

    public void setVideos(Set<VideoViewModel> videos) {
        this.videos = videos;
    }
}
