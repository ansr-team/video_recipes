package app.models.dto.viewModel.recipe;

import app.models.dto.viewModel.PageableViewModel;

public class RecipePerCategoryViewModel extends PageableViewModel<RecipePerCategoryViewModel> {

    private Long id;
    private String name;
    private String body;
    private String picturePath;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }
}
