package app.models.dto.viewModel.video;

import app.models.dto.viewModel.comment.VideoCommentViewModel;

import java.util.HashSet;
import java.util.Set;

public class ShowSingleVideoViewModel {

    private Long id;
    private String path;
    private Set<VideoCommentViewModel> comments=new HashSet<>(0);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Set<VideoCommentViewModel> getComments() {
        return comments;
    }

    public void setComments(Set<VideoCommentViewModel> comments) {
        this.comments = comments;
    }
}
