package app.models.dto.viewModel;

import org.springframework.data.domain.Page;

import java.util.List;

public abstract class PageableViewModel<T> {

    private Long count;
    private List<Integer> nextPages;
    private List<Integer> previousPages;
    private int currentPage;
    private Page<T> page;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<Integer> getNextPages() {
        return nextPages;
    }

    public void setNextPages(List<Integer> nextPages) {
        this.nextPages = nextPages;
    }

    public List<Integer> getPreviousPages() {
        return previousPages;
    }

    public void setPreviousPages(List<Integer> previousPages) {
        this.previousPages = previousPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }

}
