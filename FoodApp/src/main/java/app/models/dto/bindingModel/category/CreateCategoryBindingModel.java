package app.models.dto.bindingModel.category;

import app.models.dto.bindingModel.file.UploadFileBindingModel;
import org.springframework.web.multipart.MultipartFile;

public class CreateCategoryBindingModel extends UploadFileBindingModel{

    private String name;
    private MultipartFile upload;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getUpload() {
        return upload;
    }

    public void setUpload(MultipartFile file) {
        this.upload = file;
    }
}
