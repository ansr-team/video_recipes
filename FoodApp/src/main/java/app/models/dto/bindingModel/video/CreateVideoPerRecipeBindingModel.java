package app.models.dto.bindingModel.video;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CreateVideoPerRecipeBindingModel extends CreateVideoBindingModel {

    private String name;
    private String linkId;
    private List<Long> recipesIds = new ArrayList<>(0);
    private List<String> tagNames = new ArrayList<>(0);

    public CreateVideoPerRecipeBindingModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public List<Long> getRecipesIds() {
        return recipesIds;
    }

    public void setRecipesIds(List<Long> recipesIds) {
        this.recipesIds = recipesIds;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }
}
