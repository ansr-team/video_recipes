package app.models.dto.bindingModel.search;

import java.util.ArrayList;
import java.util.List;

public class SearchOrderedBindingModel {

    private static final int DEFAULT_PAGE_NUM = 0;

    private String searchKeyWords;
    private String order = "";
    private List<String> categoryNames = new ArrayList<>(0);
    private int videoPage = DEFAULT_PAGE_NUM;
    private int recipePage = DEFAULT_PAGE_NUM;

    public String getSearchKeyWords() {
        return searchKeyWords;
    }

    public void setSearchKeyWords(String searchKeyWords) {
        this.searchKeyWords = searchKeyWords;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public List<String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(List<String> categoryNames) {
        this.categoryNames = categoryNames;
    }

    public int getVideoPage() {
        return videoPage;
    }

    public void setVideoPage(int videoPage) {
        this.videoPage = videoPage;
    }

    public int getRecipePage() {
        return recipePage;
    }

    public void setRecipePage(int recipePage) {
        this.recipePage = recipePage;
    }
}
