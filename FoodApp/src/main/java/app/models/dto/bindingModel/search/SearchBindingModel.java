package app.models.dto.bindingModel.search;

public class SearchBindingModel {

    private static final int DEFAULT_PAGE_NUM = 0;

    private String searchKeyWords;

    private int videoPage = DEFAULT_PAGE_NUM;

    private int recipePage = DEFAULT_PAGE_NUM;

    public int getVideoPage() {
        return videoPage;
    }

    public void setVideoPage(int videoPage) {
        this.videoPage = videoPage;
    }

    public int getRecipePage() {
        return recipePage;
    }

    public void setRecipePage(int recipePage) {
        this.recipePage = recipePage;
    }

    public String getSearchKeyWords() {
        return searchKeyWords;
    }

    public void setSearchKeyWords(String searchKeyWords) {
        this.searchKeyWords = searchKeyWords;
    }
}
