package app.models.dto.bindingModel.comment;

public class CreateVideoCommentBindingModel {


    private String body;


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
