package app.models.dto.bindingModel.file;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UploadFileBindingModel {

    private MultipartFile upload;

    public MultipartFile getUpload() {
        return upload;
    }

    public void setUpload(MultipartFile file) {
        this.upload = file;
    }
}
