package app.models.dto.bindingModel.category;

import org.springframework.web.multipart.MultipartFile;

public class EditCategoryBindingModel {

    private Long id;
    private String name;
    private MultipartFile file;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getFile() {
        return this.file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
