package app.models.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(indexes = {@Index(name = "tag_name_index",columnList = "name")})
public class Tag {

    private Long id;
    private String name;
    private Set<Video> videos = new HashSet<>(0);
    private Set<Recipe> recipes = new HashSet<>(0);
    private Set<Article> articles = new HashSet<>(0);

    public Tag() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "tags", cascade = CascadeType.ALL)
    public Set<Video> getVideos() {
        return videos;
    }

    public void setVideos(Set<Video> videos) {
        this.videos = videos;
    }

    @ManyToMany(mappedBy = "tags", cascade = CascadeType.ALL)
    public Set<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(Set<Recipe> recipes) {
        this.recipes = recipes;
    }

    @ManyToMany(mappedBy = "tags", cascade = CascadeType.ALL)
    public Set<Article> getArticles() {
        return articles;
    }

    public void setArticles(Set<Article> articles) {
        this.articles = articles;
    }
}
