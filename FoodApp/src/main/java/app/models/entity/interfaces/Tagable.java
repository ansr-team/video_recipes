package app.models.entity.interfaces;

import app.models.entity.Tag;

import java.util.Set;

public interface Tagable {

    Set<Tag> getTags();

    void setTags(Set<Tag> tags);
}
