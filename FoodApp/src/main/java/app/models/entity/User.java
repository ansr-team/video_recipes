package app.models.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User {

    private Long id;
    private String name;
    private String password;
    private String accessToken;
    private Set<Authority> authorities = new HashSet<>(0);
    private boolean hasValidToken;

    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.setHasValidToken(true);
        this.accessToken = accessToken;
    }

    public boolean getHasValidToken() {
        return hasValidToken;
    }

    public void setHasValidToken(boolean hasValidToken) {
        this.hasValidToken = hasValidToken;
    }
}
