package app.models.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Picture {

    private Long id;
    private String path;
    private Set<Category> categories = new HashSet<>(0);
    private Set<Recipe> recipes = new HashSet<>(0);
    private Set<Article> articles = new HashSet<>(0);

    public Picture() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @OneToMany(mappedBy = "picture",cascade = CascadeType.ALL)
    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    @OneToMany(mappedBy = "picture", cascade = CascadeType.ALL)
    public Set<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(Set<Recipe> recipes) {
        this.recipes = recipes;
    }

    @OneToMany(mappedBy = "picture", cascade = CascadeType.ALL)
    public Set<Article> getArticles() {
        return articles;
    }

    public void setArticles(Set<Article> articles) {
        this.articles = articles;
    }
}
