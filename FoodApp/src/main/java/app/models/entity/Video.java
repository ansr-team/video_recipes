package app.models.entity;

import app.models.entity.interfaces.Tagable;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(indexes = {@Index(name = "video_name_index", columnList = "name")})
public class Video implements Tagable {

    private Long id;
    private String name;
    private String linkId;
    private List<Category> categories = new ArrayList<>(0);
    private Recipe recipe;
    private Long rating = 0L;
    private Set<Tag> tags = new HashSet<>(0);
    private Date createdOn;

    public Video() {
    }

    public Video(Long id,
                 String name,
                 String linkId,
                 Long rating,
                 Date createdOn) {
        this.id = id;
        this.name = name;
        this.linkId = linkId;
        this.rating = rating;
        this.createdOn = createdOn;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @OneToOne(cascade = CascadeType.PERSIST)
    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
