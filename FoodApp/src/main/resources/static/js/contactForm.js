class ContactFormValidator {


    checkNameForm(event) {
        let name = $('#name').val();
        if (name.trim().length == 0) {
            $('label.name').find('span').eq(0).css('overflow', 'visible');
            event.preventDefault();

        } else if (name.trim().length < 3) {

            $('label.name').find('span').eq(1).css('overflow', 'visible');
            event.preventDefault();
        }
    }

    checkEmailForm(event) {
        let email = $('#email').val();
        let validateEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (email.trim().length == 0) {
            $('label.fromEmail').find('span').eq(0).css('overflow', 'visible');
            event.preventDefault();
        } else if (!validateEmail.test(email)) {
            $('label.fromEmail').find('span').eq(1).css('overflow', 'visible');

            event.preventDefault();
        }
    }

    checkPhoneForm(event) {
        let number = $('#phone').val();
        let validatePhone = /^(()?\d{3}())?(-|\s)?\d{3}(-|\s)?\d{4}$/;
        if (number.trim().length == 0) {

            $('label.phone').find('span').eq(0).css('overflow', 'visible');

            event.preventDefault();
        } else if (!validatePhone.test(number)) {
            $('label.phone').find('span').eq(1).css('overflow', 'visible');
            event.preventDefault();
        }
    }

    checkMessage(event) {
        let message=$('#message').val();
        if(message.trim().length==0){
            $('label.message').find('span').eq(0).css('overflow', 'visible');
            event.preventDefault();
        }else if(message.trim().length<5){
            $('label.message').find('span').eq(1).css('overflow', 'visible');
            event.preventDefault();
        }

    }

    hideALlMessages() {
        $('label').find('span').css('overflow', 'hidden');
    }

    validateContactForm() {
        let self = this;
        $('#submitEmail').click(function (event) {
            self.hideALlMessages();
            self.checkNameForm(event);
            self.checkEmailForm(event);
            self.checkMessage(event);
        })
    }
}

$(document).ready(function () {
    let validator = new ContactFormValidator();
    validator.validateContactForm();
});