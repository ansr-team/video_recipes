class EmbedVideo {

    /**
     * @param {Requester}requester
     * @param {YouTubeVideoTitleRequester} youtubeApi
     */

    constructor(requester, youtubeApi) {
        this._width = 350;
        this._height = 200;
        this._requester = requester;
        this._youtubeApi = youtubeApi;
        this._tag = document.createElement('script');
        this._tag.src = "https://www.youtube.com/iframe_api";
        this._firstScriptTag = document.getElementsByTagName('script')[0];
        this._firstScriptTag.parentNode.insertBefore(this._tag, this._firstScriptTag);
        this.done = false;
    }


    onMyYouTubeIframeAPIReady(divIframeId) {

        let self = this;
        this._requester.get("/allVideoIds", function (videoLinkIds) {
            let videoIds = JSON.parse(videoLinkIds);

            for (let i = 0; i < videoIds.length; i++) {

                if (divIframeId == videoIds[i].recipeId + 'pic') {
                    self.createYPlayer(divIframeId, videoIds[i].linkId, videoIds[i].recipeId)
                }
            }
        });
    }

    createYPlayer(divIframeId, videoLinkId, videoRecipeId) {
        let self = this;

        $.getScript("https://www.youtube.com/iframe_api", function () {
            this.playyyerer = new YT.Player(divIframeId, {

                height: self._height,
                width: self._width,
                autoplay: 1,

                playerVars: {

                    controls: 0,
                    showinfo: 0,
                    modestbranding: 0,
                    autohide: 0
                },
                videoId: videoLinkId,
                events: {

                    'onReady': (e) => {
                        let pic = divIframeId.replace('pic', '');
                        var el = $(e)[0].target;
                        var el1 = $(el)[0].a;
                        e.target.playVideo();

                        e.target.mute();

                        $(el1).mouseenter(function (ev) {
                            let pic = divIframeId.replace('pic', '')
                            $('#' + pic).fadeOut('slow');
                            $('#' + divIframeId).fadeIn('slow');
                            e.target.playVideo();

                            e.target.mute();
                            var c = 1;
                        });
                        $(el1).mouseleave(function () {
                            e.target.stopVideo();
                            let pic = divIframeId.replace('pic', '')
                            $('#' + pic).fadeIn('slow');

                        });

                    },
                    'onStateChange': (e)=> {
                        if (e.data == YT.PlayerState.PAUSED) {
                            var el = $(e)[0].target;
                            let allVideos = ($('.thumbnail__video'));
                            self.findWichVideoIsClicked(allVideos, videoLinkId, videoRecipeId)
                            self.done = true;
                        }
                    }
                }

            });
        })

    }

    findWichVideoIsClicked(allVideos, videoLinkId, videoRecipeId) {
        let self = this;
        $.each(allVideos, function (key, value) {
            let clickedVideo = value.href.substr(value.href.indexOf('embed/') + 6);

            if (clickedVideo == videoLinkId) {
                let clickedRecipeId = ($($(this).children()[0]).attr('id'));
                if (clickedRecipeId == videoRecipeId + 'pic') {
                    $(value).trigger('click');
                    self._requester.get('/video/' + videoRecipeId, function (name) {
                        name = JSON.parse(name);

                        if($('.customPlayer').length==0){
                            $('.fancybox-skin').prepend('<a class="customPlayer" style="padding-bottom: 4px;" id="videoTitle" href="/recipes/' + videoRecipeId + '">' + name + '</a>');
                            self._youtubeApi.removeDefaultVideoTitleFromFancyBoxPlayer();
                        }

                        return
                    })
                }
            }

        });
    }


}