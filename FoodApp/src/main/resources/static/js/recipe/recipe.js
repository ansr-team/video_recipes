class Recipe {

    /**
     *
     * @param {Query}query
     * @param {Client} client
     */
    constructor(query, client) {
        this._query = query;
        this._client = client;
    }

    /**
     * @param {Event}event
     */
    setEvent(event) {
        this._event = event;
    }

    getCreateRecipeModal(event) {

        if ($('#recipeModalView').length === 0) {
            this._query.getCreateRecipeModal(event)
                .then((modal)=>this._client.getCreateRecipeModal(modal))
                .then(()=>this._event.onAddVideoLinkOnCreateRecipeModalClicked())
        } else {
            this._client.showModal('#recipeModalView');
        }
    }

    addVideoLinkOnCreateRecipeModal(event) {
        event.preventDefault();
        this._client.appendCreateVideoDivOnRecipeModal()
    }
    addRecipeNameOnVideosOnHomePage(recipedId){
        this._query.getRecipeName(recipedId)
            .then((name)=>this._client.appendVideoNameOnThumbNailOnHomePage(recipedId,name))
    }

}
