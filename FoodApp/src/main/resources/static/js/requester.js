class Requester {
    constructor() {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        this._headers = {};
        this._headers[header] = token;
    }

    get(url,successCallback,errorCallback,requestData) {
        errorCallback = errorCallback || function(error){};
        $.ajax({
            method:"GET",
            url:url,
            data:requestData,
            headers:this._headers,
            success:successCallback,
            error:errorCallback
        })
    }

    post(url,requestData,successCallback,errorCallback){
        errorCallback = errorCallback || function(error){};
        $.ajax({
            method:'POST',
            url:url,
            data:requestData,
            headers:this._headers,
            success:successCallback,
            error:errorCallback
        })
    }
}