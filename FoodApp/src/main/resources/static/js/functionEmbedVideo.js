let requester = new Requester();
let youtubeApi = new YouTubeVideoTitleRequester();
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
let id = $('img.alignnone').attr('id');

function onYouTubeIframeAPIReady() {
    $('.lazy-img').hide();

    requester.get("/allVideoIds", function (videoLinkIds) {
        let videoIds = JSON.parse(videoLinkIds);


        for (let i = 0; i < videoIds.length; i++) {

            if (id == videoIds[i].recipeId + 'pic') {

                $('a.thumbnail__video').show();
                player = new YT.Player(id, {

                    height: '350',
                    width: '100%',
                    autoplay: 1,

                    playerVars: {
                        // Auto-play the video on load
                        controls: 0,        // Show pause/play buttons in player
                        showinfo: 0,  // Hide the video title
                        modestbranding: 0,  // Hide the Youtube Logo
                        //                            loop: 1,            // Run the video in a loop
                        //                            fs: 0,              // Hide the full screen button
                        //                            cc_load_policy: 0, // Hide closed captions
                        //                            iv_load_policy: 3,  // Hide the Video Annotations
                        autohide: 0        // Hide video controls when playing
                    },
                    videoId: videoIds[i].linkId,
                    events: {

                        'onReady': (e) => {

                            var el = $(e)[0].target;
                            var el1 = $(el)[0].a;
                            e.target.playVideo();

                            e.target.mute();
                            $(el1).mouseenter(function (ev) {
                                e.target.playVideo();

                                e.target.mute();
                                // console.log(e);
                                var c = 1;
                            });
                            $(el1).mouseleave(function () {

                                e.target.stopVideo();
                            })

                        },
                        'onStateChange': (e)=> {
                            if (e.data == YT.PlayerState.PAUSED) {

                                var el = $(e)[0].target;

                                let allVideos = ($('.thumbnail__video'));

                                $.each(allVideos, function (key, value) {
                                    let clickedVideo = value.href.substr(value.href.indexOf('embed/') + 6);

                                    if (clickedVideo == videoIds[i].linkId) {
                                        let clickedRecipeId = ($($(this).parent().first()[0]).children().first().attr('id'));

                                        if (clickedRecipeId == videoIds[i].recipeId) {
                                            $(value).trigger('click');
                                            requester.get('/video/' + videoIds[i].recipeId, function (name) {

                                                name = JSON.parse(name);
                                                $('.fancybox-skin').prepend('<a class="customPlayer" style="color:goldenrod;padding-bottom: 10px;" id="videoTitle" href="/recipes/' + videoIds[i].recipeId + '">' + name + '</a>');
                                                youtubeApi.removeDefaultVideoTitleFromFancyBoxPlayer();
                                                return
                                            })
                                        }


                                    }

                                });

                                done = true;
                            }
                        }
                    }

                });
                break;

            }
        }


    });


}


var done = false;
function stopVideo() {

    player.stopVideo();
}