class Page {
    /**
     * @param {Requester}requester
     * @param {Client} client
     */

    constructor(requester, client) {
        this._requester = requester;
        this._client = client;

    }

    onPageClick(startPage, event, url) {

        this._pageToSend = startPage;

        if (event) {
            if (!$(event.target).hasClass("disable")) {
                this._pageToSend = $(event.target).data("next-page");
            }
        }
        let path = url + "/pageable?page=" + this._pageToSend;
        if (this._pageToSend !== null) {
            let promise = $.Deferred(this._requester.get(path, function (data) {

                promise.resolve(data)
            }));
            return promise.promise();
        }
    };

    /**
     * @param{Event} event
     */
    setEvent(event) {
        this._event = event;

    }

    triggerPagination(startPage, event, url) {

        let promise = this.onPageClick(startPage, event, url);
        if (promise) {
            promise.then((data)=>this._client.appendNextPage(data,startPage))
                .then(()=> this._event.onNextPageClicked(url));
        }

    }
}