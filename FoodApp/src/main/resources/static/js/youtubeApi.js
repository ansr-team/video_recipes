class YouTubeVideoTitleRequester {

    constructor(){
        this._apiKey='AIzaSyBDQzdBrLYjD0rtviu-2JMPVYbt1v64uqU';
    }


    getYouVideoTitle(videoId) {
        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoId + "&key=" + this._apiKey + "&fields=items(snippet(title))&part=snippet",
            dataType: "jsonp",
            success: function (data) {
                return (data.items[0].snippet.title);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus, +' | ' + errorThrown);
            }
        });
    }
    setYouVideoTitle(videoId,JqueryElement) {
        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoId + "&key=" + this._apiKey + "&fields=items(snippet(title))&part=snippet",
            dataType: "jsonp",
            success: function (data) {
                let name= (data.items[0].snippet.title);
                JqueryElement.val(name);
                $('#videoName').attr('size',name.length)

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus, +' | ' + errorThrown);
            }
        });
    }

    removeDefaultVideoTitleFromFancyBoxPlayer(){
        $($('.fancybox-inner').children()).attr('src');
        let b=$($('.fancybox-inner').children()).attr('src');
        b=b.replace('autoplay=1','autoplay=1&showinfo=0');
        $($('.fancybox-inner').children()).attr('src',b)
    }
}