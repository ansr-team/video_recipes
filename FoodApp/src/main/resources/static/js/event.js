class Event {

    /**
     *
     * @param {Query}query
     * @param {Client} client
     * @param {Video} video
     * @param {Recipe} recipe
     * @param {Category}category
     * @param {Page} page
     * @param {YouTubeVideoTitleRequester} yap;
     */

    constructor(query, client, video, recipe, category, page, yap) {
        this._query = query;
        this._client = client;
        this._video = video;
        this._recipe = recipe;
        this._category = category;
        this._page = page;
        this._yap = yap;
        $('body').css('background', '#fffae6');

    }

    bindAllEvents() {
        this.onWindowScroll();
        this.onWindowClick();
        this.onVideoClicked();
        this.onCreateRecipeModalClicked();
        this.onAddVideoLinkOnCreateRecipeModalClicked();
     //   this.onCategoriesDropdownClicked();
        this.onCategoriesDropdownStuckHeaderClicked();
        this.onVideoTumbNailHovered();
        this.onThumbNailCClicked()
        this.onAroundCategoryDropDownCLicked();

    }

    onAroundCategoryDropDownCLicked() {
        let self = this;

        $('.btn-circle').click(function (e) {
            if ($('#myDropdown').length == 0) {
                $('a.CategoryDown').triggerHandler('click', self._category.appendCategoriesPartialView(this));
            }else {
                $('#myDropdown').remove()
            }

        })
    }

    onVideoClicked() {
        $('.video').click((event)=>this._query.getSingleVideoInScreen(event));
        $('#SubmitComment').click((event)=>this._video.addVideoComment(event));
    }

    onCreateRecipeModalClicked() {
        $('#recipeModalButton').click((event)=> this._recipe.getCreateRecipeModal(event));

    }

    onAddVideoLinkOnCreateRecipeModalClicked() {

        $('#addVideo').click((event)=>this._recipe.addVideoLinkOnCreateRecipeModal(event))
    }

    onCategoriesDropdownClicked() {
        let self = this;
        $('.CategoryDown').click(function () {
            if ($('#myDropdown').length == 0) {
                self._category.appendCategoriesPartialView(this)
            } else {
                $('#myDropdown').remove()
            }

        })


    }

    onCategoriesDropdownStuckHeaderClicked() {

    }

    onWindowClick() {
        $(window).click(function () {
                if ($('#myDropdown').length !== 0) {
                    $('#myDropdown').remove()
                    $('#myDropdown').remove()
                }

            }
        );
    }

    onWindowScroll() {
        $(window).scroll(function (e) {
            let $stuckContainer = $('.stuck_container.isStuck');
            let $dropdownContent = $stuckContainer.find('.dropdown-content');

            if ($stuckContainer.attr('isSlideTriggered') === 'true') {
                $dropdownContent.hide();
                $dropdownContent.prev('.dropbtn').attr('toggled', 'false');
            }
        })
    }

    onVideoTumbNailHovered() {
        let self = this;
        $('.thumbnail_overlay').hover(function () {
            if ($('#toTop').hasClass('fancybox-margin')) {
                self._yap.removeDefaultVideoTitleFromFancyBoxPlayer()
            }
        })
    }

    onNextPageClicked(url) {

        let self = this;
        let allPages = $('a.page')
        $(".page").click(function (event) {
            let nextPage = Number($(this).attr('data-next-page'));
            if (nextPage < 0 || nextPage > allPages.size() - 3) {
                //  event.preventDefault();
                return;
            }
            self._page.triggerPagination(nextPage, event, url)
        });
    }

    onThumbNailCClicked() {
        let self = this;
        $('.thumbnail__video').click(function () {
            let path = window.location.pathname;
            if (path === "/") {
                let recipeId = ($(this).parent().attr('id'));
                self._recipe.addRecipeNameOnVideosOnHomePage(recipeId);
            }
        })
    }
}
