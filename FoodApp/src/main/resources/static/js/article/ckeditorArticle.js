function loadCkEditor() {
    CKEDITOR.replace('ck_editor', {
        //TODO: maybe it is good to get the path by constants
        filebrowserImageUploadUrl: '/articles/picture/upload',
        height: '100px'

    });
    loadFiles();
    // CKEDITOR.config.contentsHeight

}

function loadFiles() {
    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName == 'image') {
            dialogDefinition.onLoad = function () {
                var dialog = CKEDITOR.dialog.getCurrent();

                var uploadTab = dialogDefinition.getContents('Upload');
                var uploadButton = uploadTab.get('uploadButton');
            };
        }
    });
}
$.getScript("//cdn.ckeditor.com/4.5.11/standard/ckeditor.js", function(data) {
    $(document).ready(loadCkEditor);
});
