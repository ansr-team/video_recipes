"use strict";

class Category {

    constructor(requester, query, client) {
        this._requester = requester;
        this._query = query;
        this._client = client;
        triggerSlidebarAppending();
    }

    appendCategoriesPartialView(context) {
        let self = this;
        self._query.getCategoriesDropdownPartialView()
            .then(function (data) {

                self._client.appendCategoriesDropdown(data,context).then(function (dat) {

                    dat = $(dat);
                    $(window).on("scroll", function () {
                        $($('.img-circle-div')).css('visibility', 'visible');

                        let x = Number.parseInt($(window).scrollTop() + $(window).height());
                        let y = Number.parseInt($(document).height());

                        var $doc = $(document);
                        var $win = $(window);
                        if (x < 1068) {
                            $('.partial').hide();
                            $('#myDropdown').remove()
                        }
                        if ($(window).scrollTop() == 0) {
                            $('.partial').hide();
                            $('#myDropdown').remove()
                        } else if (x == y || x == y - 1) {
                            //second case is when broweser is resized

                            $('.partial').hide();
                            $('#myDropdown').remove()

                        }
                    })
                })
            })
    };
}