var resourcesPath = "/js/theme/";
function include(url) {
    document.write('<script src="' + url + '"></script>');
    return false;
}
/* cookie.JS
 ========================================================*/
include(resourcesPath + 'jquery.cookie.js');


/* DEVICE.JS
 ========================================================*/
include(resourcesPath + 'device.min.js');

/* Stick up menu
 ========================================================*/
include(resourcesPath + 'tmstickup.js');
function triggerSlidebarAppending() {
    let deffer = $.Deferred();
    if ($('html').hasClass('desktop')) {
        $('#stuck_container').TMStickUp({})
        deffer.resolve();
    }

    return deffer.promise;
}

/* Stellar.js
 ========================================================*/
include(resourcesPath + 'jquery.stellar.js');
$(document).ready(function () {
    if ($('html').hasClass('desktop')) {
        $.stellar({
            horizontalScrolling: false
        });
    }
});


/* Easing library
 ========================================================*/
include(resourcesPath + 'jquery.easing.1.3.js');

/* ToTop
 ========================================================*/
include(resourcesPath + 'jquery.ui.totop.js');
$(function () {
    $().UItoTop({easingType: 'easeOutQuart'});
});


/* DEVICE.JS AND SMOOTH SCROLLIG
 ========================================================*/
include(resourcesPath + 'jquery.mousewheel.min.js');
include(resourcesPath + 'jquery.simplr.smoothscroll.min.js');
$(function () {
    if ($('html').hasClass('desktop')) {
        $.srSmoothscroll({
            step: 150,
            speed: 800
        });
    }
});

/* Copyright Year
 ========================================================*/
var currentYear = (new Date).getFullYear();
$(document).ready(function () {
    $("#copyright-year").text((new Date).getFullYear());
});


/* Superfish menu
 ========================================================*/
include(resourcesPath + 'superfish.js');
include(resourcesPath + 'jquery.mobilemenu.js');

/* Unveil
 ========================================================*/
include(resourcesPath + 'jquery.unveil.js');
$(document).ready(function () {
    $(".lazy-img").find('img').unveil(300, function () {
        $(this).load(function () {
            $(this).addClass("lazy-loaded");
        });
    });
});

/* Google Map
 ========================================================*/
$(window).load()
{
    if ($('#google-map').length > 0) {
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(parseFloat(40.646197), parseFloat(-73.9724068, 14)),
            scrollwheel: false
        }
        new google.maps.Map(document.getElementById("google-map"), mapOptions);
    }
}

/* Orientation tablet fix
 ========================================================*/
$(function () {
    // IPad/IPhone
    var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
        ua = navigator.userAgent,

        gestureStart = function () {
            viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
        },

        scaleFix = function () {
            if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
                viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
                document.addEventListener("gesturestart", gestureStart, false);
            }
        };

    scaleFix();
    // Menu Android
    if (window.orientation != undefined) {
        var regM = /ipod|ipad|iphone/gi,
            result = ua.match(regM);
        if (!result) {
            $('.sf-menu li').each(function () {
                if ($(">ul", this)[0]) {
                    $(">a", this).toggle(
                        function () {
                            return false;
                        },
                        function () {
                            window.location.href = $(this).attr("href");
                        }
                    );
                }
            })
        }
    }
});
var ua = navigator.userAgent.toLocaleLowerCase(),
    regV = /ipod|ipad|iphone/gi,
    result = ua.match(regV),
    userScale = "";
if (!result) {
    userScale = ",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0' + userScale + '">')

$(document).ready(function () {
    var obj;

    if ((obj = $('#camera')).length > 0) {
        obj.camera({
            autoAdvance: true,
            height: '33.75%',
            minHeight: '280px',
            pagination: false,
            thumbnails: false,
            playPause: false,
            hover: false,
            loader: 'none',
            navigation: true,
            navigationHover: false,
            mobileNavHover: false,
            fx: 'simpleFade',

        })
    }

    var isotope;
    if ((isotope = $('#isotope')).length > 0) {

        isotope.isotope({
            itemSelector: '.item',
            transitionDuration: '1s',
            hiddenStyle: {
                opacity: 0
            },
            visibleStyle: {
                opacity: 1
            },
            masonry: {
                columnWidth: 2,
                gutter: 0
            }
        });

        $('#isotope-filters').on('click', 'a', function () {
            var filterValue = $(this).attr('data-filter');

            if (filterValue == '*') {
                isotope.removeClass('filtered');
                isotope.isotope({filter: filterValue});
            } else {
                isotope.addClass('filtered');
                isotope.isotope({filter: '.' + filterValue});
            }


            $('#isotope-filters').find('li').removeClass('active');
            $(this).parent().addClass('active');
            return false;
        });
    }

    if ((obj = $('.thumbnail')).length > 0 && !obj.hasClass('thumbnail__video')) {
        if (obj) {
            if (obj.hasOwnProperty('touchTouch')) {
                if (typeof obj.touchTouch == 'function') {
                    obj.touchTouch();
                }else {
                    console.log('touchtouch is not Function');
                }
            }else {
                console.log('touchtouch is not property');
            }

        }
    }

    if ((obj = $('.thumbnail__video')).length > 0) {
        obj.fancybox()
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                prevEffect: 'none',
                nextEffect: 'none',

                arrows: false,
                helpers: {
                    media: {},
                    buttons: {}
                }
            });
    }

});
