class Client {


    constructor() {
        this._youtubeApi = new YouTubeVideoTitleRequester();

    }

    goToSingleVideoPage(event) {
        event.preventDefault();
        let videoId = event.target.id;
        window.location.replace("http://localhost:8080/1/videos/" + videoId);

    }

    addVideoComment(comment) {
        let commentFormTextArea = $('#comment');
        commentFormTextArea.val("");
        let newComment = JSON.parse(comment);
        let divComment = $('<div></div>');
        let divBody = $('<div>' + newComment.body + '</div>');
        let divDate = $('<div id=' + newComment.id + '>' + newComment.createdOn + '</div>');
        divComment.append(divBody);
        divComment.append(divDate);
        $('#Comments').append(divComment);
    }

    getCreateRecipeModal(modal) {
        let recipeModal = '#recipeModal';
        let recipeModalView = '#recipeModalView';
        this.appendModal(recipeModal, modal);
        this.showModal(recipeModalView);

        return $.Deferred().resolve().promise();
    }

    appendModal(idElement, modal) {
        $(idElement).append(modal);
    }

    showModal(idElement) {
        $(idElement).modal({
            show: 'true',
            keyboard: true,
            backdrop: 'static'
        });
    }

    appendCreateVideoDivOnRecipeModal() {
        let toAppend = ' <div>' +
            '  <label>Paste video URL: </label>' +
            '<input th:type="text" th:field="*{linkId}" th:id="url"/>'
        ' <p th:if="${#fields.hasErrors(\'linkId\')}" th:errors="*{linkId}"></p>' +
        ' </div>';

        $('#toAppendVideoLink').append(toAppend);
    }

    appendCategoriesDropdown(data, context) {

        $(context).append(data);
        $('#myDropdown').show();
        let promise = $.Deferred().resolve(data);

        return promise.promise();
    }

    appendNextPage(data, startPage) {

        if ($('.articlePreviewsSection').position()) {
            let coord = $('.articlePreviewsSection').position().top;
            $(window).scrollTop(coord);

        }

        $("#page-container").html(data);
        
        let pages = $('a.page');

        pages.each(function (index, page) {

            startPage = Number(startPage);

            let pageNumber = Number($(page).attr('data-next-page'));
            if (pageNumber == 0 || pageNumber == pages.size() - 3){
                if($(page).text() != 'Previous' && $(page).text() != 'Next') {
                    $(page).css('cssText','color: #0000FF !important;');
                    $(page).parent().show();
                }
            }

            if (((pageNumber) < (startPage) - 1  )) {

                if (pageNumber != 0 || pageNumber != pages.size) {
                    $(page).parent().hide();
                }
                if (pageNumber == 0 || pageNumber >= pages.size() - 3) {
                    $(page).parent().show();
                }

            } else if ((pageNumber) > (startPage) + 1) {
                {
                    $(page).parent().hide();
                    if (pageNumber == 0 || pageNumber >= pages.size() - 3) {
                        $(page).parent().show();
                    }

                }
            }
        });
        return $.Deferred().resolve().promise();
    }

    appendVideoNameOnThumbNailOnHomePage(recipeId, name) {

        if($('.customPlayer').length==0){
            recipeId=recipeId.replace('pic','');
            $('.fancybox-skin').prepend('<a class="customPlayer" style="padding-bottom: 4px" id="videoTitle" href="/recipes/' + recipeId + '">' + name + '</a>');
            this._youtubeApi.removeDefaultVideoTitleFromFancyBoxPlayer();

        }
    }
}