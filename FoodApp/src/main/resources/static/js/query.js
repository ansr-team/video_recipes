class Query {
    /**
     *
     * @param {Requester}requester
     */
    constructor(requester) {
        this._requester = requester;
    }

    getSingleVideoInScreen(event) {
        let videoId = event.currentTarget.id;
        window.location = "/food-web/videos/" + videoId;
    }



    getCreateRecipeModal(event) {
        let promise = $.Deferred(this._requester.get("/recipes/create", function (modal) {
            promise.resolve(modal)
        }));
        return promise.promise()
    }



    getCategoriesDropdownPartialView() {
        let promise = $.Deferred(
            this._requester.get('/categories/toggle', function (data) {
            promise.resolve(data);
        }));

        return promise.promise();
    }
    getRecipeName(recipeId){

        let promise=$.Deferred( this._requester.get('/video/' +recipeId, function (name) {

            name = JSON.parse(name);
           promise.resolve(name);
        }));
        return promise.promise();
    }
}