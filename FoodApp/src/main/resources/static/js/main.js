$(document).ready(function () {
    let requester = new Requester();
    let client = new Client();
    let page = new Page(requester, client)
    let event = new Event(null, null, null, null, null, page);
    page.setEvent(event);
    let url = "/blog";
    page.triggerPagination(0, null, url);
});