$(document).ready(function () {
    let requester = new Requester();
    let query = new Query(requester);
    let client = new Client();
    let recipe = new Recipe(query, client);
    let category = new Category(requester, query, client);
    let yAp = new YouTubeVideoTitleRequester();
    let event = new Event(query, client, null, recipe, category,null, yAp);
    event.bindAllEvents();

});